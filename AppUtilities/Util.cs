﻿using HyProvider.HDS;
using HyAdmin.Services;
using HyProvider.SMSService;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.AppUtilities
{
    public static class Util
    {

        public static string Receipt { get; set; }
        public static string AbsoluteURL { get; } = ConfigurationManager.AppSettings["AbsoluteURL"];

        public static string ErrorReportingSubject { get; set; } = "HyDirectAdmin Error Reporting";

        public static bool isDevelopmentMode { get; set; } = (ConfigurationManager.AppSettings["env"] == "development");

        public static string HyDirectV3 { get; internal set; } = (isDevelopmentMode) ? ConfigurationManager.AppSettings["hydirectDev"] : ConfigurationManager.AppSettings["hydirectProd"];

        internal static void LogException(Exception e, string action, string controller, string url, string IP, string macAddr)
        {
            WriteErrorLog($"Action: {action}\n\rController: {controller}\n\rURL: {url}\n\rIP: {IP}\n\rMac Address: {macAddr}\n\r\rException Details:");
            WriteErrorLog(e);
        }

        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\HyDirectAdmin.csv", true);
                sw.WriteLine(DateTime.Today.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) +
                    ": " + ex.Source.ToString().Trim() + ": " + ex.Message.ToString().Trim() + "\n" + ex.InnerException);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteErrorLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\HyDirectAdmin.csv", true);
                sw.WriteLine(DateTime.Today.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "abcdefghijklmopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-.*";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        internal static string GetIpAddres()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public static byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
        
     
        public static string ReturnRoot()
        {
            return ConfigurationManager.AppSettings["root"].ToString();
        }

        public static string ReturnHost()
        {
            return ConfigurationManager.AppSettings["AbsoluteURL"];
        }
        
        public static string GetRoleName(int RoleTypeId)
        {
            switch (RoleTypeId)
            {
                case 0:
                    return "Enrollee";
                case 1:
                    return "Provider";
                case 2:
                    return "Group";
                case 3:
                    return "Admin";
                default:
                    return "Individual";
            }
        }

        internal static string GetAbsolutePicture(string pixpath)
        {
            if (pixpath != null && pixpath.Contains("R:"))
            {
                string[] picpath = pixpath.Split('\\');
                string image = picpath.Last();
                return $"https://online.hygeiahmo.com/hygeia-cba/uploads/enrolment-uploads/pictures/{image}";
            }
            return pixpath;
        }

        public static string DownloadMemberPhoto(string MemberID, string AbsoluteURL)
        {
            try
            {
                if (String.IsNullOrEmpty(AbsoluteURL) || AbsoluteURL == "img/default-avatar.png")
                    return "No Image";

                string extension = (AbsoluteURL.Contains(".png")) ? ".png" : ".jpg";
                string destination = $"C:\\inetpub\\wwwroot\\hygeia-cba\\uploads\\enrolment-uploads\\Pictures\\{MemberID}{extension}";


                (new System.Net.WebClient()).DownloadFile(AbsoluteURL, destination);

                return @"R:\Pictures\" + MemberID + extension;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        internal static System.Web.HttpCookie AddCookieRole(System.Web.HttpCookie defaultAccount, string role, int years)
        {
            try
            {
                System.Web.HttpContext.Current.Request.Cookies.Remove("defaultAccount");
            }
            catch
            {
            }
            defaultAccount = new System.Web.HttpCookie("defaultAccount");
            DateTime now = DateTime.Now;
            defaultAccount.Value = role;
            defaultAccount.Expires = DateTime.Now.AddYears(years);
            return defaultAccount;
        }
        
        public static string RandomNumber(int length)
        {
            Random random = new Random();
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        
        public static string FormatNaira(string input)
        {
            try
            {
                return "₦ " + String.Format("{0:C2}", Convert.ToDecimal(input)).Replace("£", "").Replace("$", "");
            }
            catch (Exception ex)
            {
                return "₦ " + input;
            }
        }

        public static bool IsValid(object value)
        {
            bool isValid = false;
            var file = value as HttpPostedFileBase;

            if (file == null || file.ContentLength > 1 * 1024 * 1024)
            {
                return isValid;
            }

            if (IsFileTypeValid(file))
            {
                isValid = true;
            }

            return isValid;
        }

        private static bool IsFileTypeValid(HttpPostedFileBase file)
        {
            bool isValid = false;

            try
            {
                using (var img = Image.FromStream(file.InputStream))
                {
                    if (IsOneOfValidFormats(img.RawFormat))
                    {
                        isValid = true;
                    }
                }
            }
            catch
            {
                //Image is invalid
            }
            return isValid;
        }

        private static bool IsOneOfValidFormats(ImageFormat rawFormat)
        {
            List<ImageFormat> formats = GetValidFormats();

            foreach (ImageFormat format in formats)
            {
                if (rawFormat.Equals(format))
                {
                    return true;
                }
            }
            return false;
        }

        private static List<ImageFormat> GetValidFormats()
        {
            List<ImageFormat> formats = new List<ImageFormat>();
            formats.Add(ImageFormat.Png);
            formats.Add(ImageFormat.Jpeg);
            formats.Add(ImageFormat.Gif);
            //add types here
            return formats;
        }

        public static int[] ConvertToIntArray(string[] arr)
        {
            int[] newArr = new int[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                try { newArr[i] = Convert.ToInt32(arr[i]); } catch { newArr[i] = -1; }
            }
            return newArr;
        }

        public static double[] ConvertToDoubleArray(string[] arr)
        {
            double[] newArr = new double[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                try { newArr[i] = Convert.ToDouble(arr[i]); } catch { newArr[i] = -1; }
            }
            return newArr;
        }

        public static string[] ConvertToStringArray(double[] arr)
        {
            string[] newArr = new string[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                try { newArr[i] = arr[i].ToString(); } catch { newArr[i] = "0"; }
            }
            return newArr;
        }

        public static string[] ConvertToStringArray(int[] arr)
        {
            string[] newArr = new string[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                try { newArr[i] = arr[i].ToString(); } catch { newArr[i] = "0"; }
            }
            return newArr;
        }

        internal static string GetTimstampNumberString()
        {
            return DateTime.Now.ToString("yyyyMMddmmhhssfffaa");
        }
        
        internal static string DBnkAcctValLink(string AcctNo, string T)
        {
            return $"https://certify.diamondbank.com/diamondconnecttest/api/Customer/accountNo/{AcctNo}";
        }
        internal static string DBnkOTPLink(string AcctNo, string T)
        {
            return $"https://certify.diamondbank.com/diamondconnecttest/api/Otp/generate/{AcctNo}";
        }

        public static string GetDiamondBankAuthentication()
        {
            var client = new RestClient("https://certify.diamondbank.com/diamondconnecttest/oauth/token");
            var request = new RestRequest(Method.POST);
            string client_id = ConfigurationManager.AppSettings["diamond_bank_client_id"];
            string api_secret = ConfigurationManager.AppSettings["diamond_bank_api_secret"];
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", $"grant_type=client_credentials&client_id={client_id}&client_secret={api_secret}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;

        }
        
        public static string GetRequestToken()
        {
            return (new Token()).Tokn;
        }

        public static string SendReceipt(string Name, string Address, string Email, string planName, double Price, double Convenience)
        {
            string total = $"{(Price + Convenience)}";

            string Receipt = $"<div class='wrapper' style='font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif; font-weight: 400;'> <div style='border: 1px solid #ccc;width: 482px;padding: 40px;'> <div> <table style='width: 100%;'> <tr> <td style='width: 55%' colspan='2'> <h4>PAYMENT RECEIPT</h4><h4>Hygeia HMO Ltd</h4> <address> 214 Broad Street,<br /> Floors 6 & 7, Elephant House,<br /> Lagos Island, Lagos<br />" + DateTime.Now.ToShortDateString() + " </address> </td></tr> </table> <br /> <table style='width: 100%;'> <tr> <td valign='top' colspan='2'> <strong>Bill To</strong> <address>";
            Receipt += $" {Name}<br />{Address} </address> <br />{DateTime.Now.ToShortDateString()}</td></tr> </table> <br /> <table style='border-collapse: collapse; border: 1px solid #CCC;width: 100%;' border='0' cellspacing='6' cellpadding='10'> <tr style='background: whitesmoke;'> <th style='padding: 10px;'>Description</th> <th style='padding: 10px;width: 150px;'>Amount</th> </tr> <tr style='background: white;border: 1px solid #ccc;'> <td style='background: white;border: 1px solid #ccc;'>{planName}</td> <td> {FormatNaira(Price.ToString())}</td> </tr> <tr style='background: whitesmoke;'> <td style='border: 1px solid #ccc;border-left: 0;' align='left'>Convenience Fee</td> <td style='border: 1px solid #ccc;'> {FormatNaira(Convenience.ToString())}</td> </tr> <tr> <td style='border-bottom: 1px solid #fff;border-left: 1px solid #fff;' align='right'><strong style='font-size:20px;'>TOTAL</strong></td> <td style='border: 1px solid #ccc;'><strong style='font-size:20px;'>{FormatNaira(total)}</strong></td> </tr> </table> </div> <br /><br /><p>Thank you for choosing Hygeia.</p> Best Regards,<br /> Hygeia </div><br /> </div>";

            IntelService s = new IntelService();
            return s.SendMail(Email, "Payment Receipt", Receipt, "", "hygeiaemail@gmail.com", 1);
        }

        public static string SendActivationCodeByMail(string ActivationCode, string URL, string name, string Email)
        {
            try
            {
                string message = $"Hi {name},<br /><br />Thank you for choosing Hygeia HMO as your healthcare provider.<br /><br />Your activation code is : {ActivationCode}<br />";
                message += $"Please follow the link to complete your registration process<br /><a href='{URL}'>{URL}</a><br /><br />Supply the activation code in the space provided and complete the form to start enjoying your healthcare benefits.<br /><br />";
                message += "Please note that your access to care will begin in 14 days.  This is to enable us properly register you across our wide hospital network.<br /><br />";
                message += $"Welcome to the Hygeia Family.<br /><br />Best Regards,<br />For Hygeia HMO";

                IntelService s = new IntelService();
                return s.SendMail(Email, "Hygeia HMO - Registration Completion", message, "", "hygeiaemail@gmail.com", 1);
            }
            catch (Exception ex)
            {
                Log(Service.MailDeliveryVerification.Code, $"Activation Code: {ActivationCode}, Email: {Email}. (Via Email)");
                return "failed";
            }
        }

        internal static void Log(string code, string message)
        {
            // Log Time
            // Log user to be reached
            // throw new NotImplementedException();
        }

        internal static void WriteErrorLog(object result)
        {
            StreamWriter sw = null;
            try
            {
                string error = JsonConvert.SerializeObject(result);
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\HyDirectWebAPI.txt", true);
                sw.WriteLine(DateTime.Today.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) +
                    ": " + error);
                sw.WriteLine("");
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void SendActivationCodeBySMS(string ActivationCode, string Phone, string URL)
        {
            string message = $"Your activation code is {ActivationCode}. Please follow the link to complete your registration process. {URL}" +
                $"Supply the activation code to continue ";
            SendSMS(Phone, message);
        }

        public static void SendSMS(string Phone, string message)
        {
            try
            {

                string msg = message;
                string sender = "Hygeia HMO";

                if (!Phone.Equals(""))
                {
                    Phone = (Phone.Trim().Length == 10) ? Phone : $"0{Phone}";
                    MessagingService sms = new MessagingService();
                    string res = sms.SendSMS("hmoHQ", "hghmo*security", sender, Phone, msg);
                }

            }
            catch (Exception ex)
            {
                Log(Service.UnknownError.Code, $"message: {message}, Phone: {Phone}. (Activation code sending failed - Via Phone)");
            }
        }
    }

    public static class APIServices
    {
        public static string BaseURL => (ConfigurationManager.AppSettings["env"] == "development") ?
            ConfigurationManager.AppSettings["api_local_root"] : ConfigurationManager.AppSettings["api_root"];

        public static string GetAuth => $"Bearer {((new Token()).GetToken("")).Tokn}";
        public static string GetAdminAuth => $"Bearer {((new Token()).GetToken("admin")).Tokn}";
        public static string GetSuperAdminAuth => $"Bearer {((new Token()).GetToken("superadmin")).Tokn}";


        internal static string Autentication => "oauth2/token";
        
        internal static string SearchProcedureByMemberProvider => "api/Procedures/SearchProcedureByMemberProvider";
        internal static string AllProcedures => "api/Procedures/AllProcedures";
        internal static string GetTariffByMemberProviderProcedure => "api/Procedures/GetTariffByMemberProviderProcedure";
        internal static string LoadCities => "api/Services/LoadCities";
        internal static string RegisterRetailEnrollee => "api/Registration/RegisterRetailEnrollee";
        internal static string PasswordSelection => "api/Provider/PasswordSelection";

        
        internal static string UserLoginData => "api/Provider/UserLoginData";

        internal static string MemberSearch => "api/Member/MemberSearch"; 
        internal static string MemberImage => "api/Member/MemberImage";
        internal static string Eligibility => "api/Eligibility/EnrolleeEligibility"; 
        internal static string CheckIn => "api/Member/InsertPrescenceConfirmationLog";
        internal static string CheckInList => "api/Provider/CheckInList";
        internal static string GetMemberIssuedPAs => "api/Member/GetMemberIssuedPAs";
        internal static string CoveredServices => "api/Member/CoveredServices";
        internal static string FindPA => "api/PA/FindPA";
        internal static string GetTariff => "api/Procedures/GetTariff";
        internal static string EnrolleeNoAuthAccess => "api/PCClaims/CheckNoAuthAccess";
        internal static string FilteredDiagnosis => "api/Diagnosis/FilteredDiagnosis";
        internal static string GetTariffWithLimits => "api/Procedures/GetTariffWithLimits";




        internal static string GetPCNSClaimData => "api/PCClaims/GetPCNSClaimData";
        internal static string GetPCNSClaimDetails => "api/PCClaims/GetPCNSClaimDetails";
        internal static string RemovePCNSClaimData => "api/PCClaims/RemovePCNSClaimData";
        internal static string ProcessBulkSerilizedPCClaims => "api/PCClaims/ProcessBulkSerilizedPCClaims";
        internal static string ProcessBulkPCClaims => "api/PCClaims/ProcessBulkPCClaims";
        


        internal static string GetPARequests => "api/PA/GetPARequests";
        internal static string GetPAIssueDetails => "api/PA/GetPAIssueDetails";
        internal static string RequestPA => "api/PA/RequestPA";
        internal static string PARulesTestWithTariff => "api/PA/PARulesTestWithTariff";
        internal static string FetchPADetails => "api/PA/FetchPADetails";
        internal static string GetCLClaimData => "api/PA/GetCLClaimData";
        internal static string GetCLNSClaimDetails => "api/PA/GetCLNSClaimDetails";
        internal static string RemoveCLNSClaimData => "api/PA/RemoveCLNSClaimData";
        internal static string PASubmission => "api/PA/PASubmission";
        internal static string BulkPAProcessing => "api/PA/BulkPAProcessing";
        

        internal static string NonSubmittedExclusions => "api/Exclusion/BeforeSubmit";
        internal static string ExclusionProcessing => "api/Exclusion/Process";


        internal static string SubmittedNoAuthReportSummary => "api/PCClaims/SubmittedNoAuthReportSummary";
        internal static string SubmittedPreAuthReportSummary => "api/PA/SubmittedPreAuthReportSummary";
        internal static string SubmittedExclusionsReportSummary => "api/Exclusions/SubmittedExclusionsReportSummary";
        
        internal static string SubmittedNoAuthReportClaims => "api/PCClaims/SubmittedNoAuthReportClaims";
        internal static string SubmittedPreAuthReportClaims => "api/PA/SubmittedPreAuthReportClaims";
        internal static string SubmittedExclusionsReportClaims => "api/Exclusions/SubmittedExclusionsReportClaims";
        internal static string SubmitExclusion => "api/Exclusion/Submit";


        internal static string GetPAClaimDenialReasons => "api/PA/GetPAClaimDenialReasons";
        internal static string GetPCClaimDenialReasons => "api/PCClaims/GetPCClaimDenialReasons";

        internal static string SubmitPCClaims => "api/PCClaims/SubmitPCClaims";
        internal static string RemovePCClaims => "api/PCClaims/RemovePCClaims";
        internal static string SendPCPaymentAdvice => "api/PCClaims/SendPCPaymentAdvice";


        internal static string SendPAPaymentAdvice => "api/PA/SendPAPaymentAdvice";
        internal static string SubmitPAClaims => "api/PA/SubmitPAClaims";

        internal static string ResetPassword => "api/Member/ResetPassword";

        
    }
    

}