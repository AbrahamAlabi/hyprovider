﻿using Microsoft.Web.WebSockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace HyProvider.Sockets
{
    public class ChatHandler : WebSocketHandler
    {
        private string userId;
        private string userName;
        private JavaScriptSerializer serializer = new JavaScriptSerializer();

        private static WebSocketCollection chatapp = new WebSocketCollection();

        public ChatHandler (string username, string providerName)
        {
            userId = username;
            userName = providerName;
        }

        public override void OnOpen()
        {
            chatapp.Add(this);
            if (String.IsNullOrEmpty(userName))
            {
                chatapp.Broadcast(serializer.Serialize(new
                {
                    type = MessageType.Join,
                    from = userId,
                    to = "all_admins",
                    value = $"{userName}[{userId}] is requesting a connection."
                }));
            }
            else
            {
                chatapp.Broadcast(serializer.Serialize(new
                {
                    type = MessageType.Join,
                    from = userId,
                    userName,
                    to = "all_admins",
                    value = $"{userName}[{userId}] is requesting a connection."
                }));
            }
        }

        public override void OnMessage(string message)
        {
            var m = serializer.Deserialize<Payload>(message);
            switch (m.Type)
            {
                case MessageType.Broadcast:
                    chatapp.Broadcast(serializer.Serialize(new
                    {
                        type = m.Type,
                        from = userId,
                        userName = (String.IsNullOrEmpty(userName) ? userId : userName),
                        to = m.To,
                        message = m.Message,
                        timeStamp = m.TimeStamp
                    }));
                    break;
                case MessageType.PickChatRequest:
                    chatapp.Broadcast(serializer.Serialize(new
                    {
                        type = m.Type,
                        from = userId,
                        userName = (String.IsNullOrEmpty(userName) ? userId : userName),
                        to = m.To,
                        message = m.Message,
                        timeStamp = m.TimeStamp
                    }));

                    // Log time CC Agent picked request
                    break;
                default:
                    return;
            }
        }

        public override void OnClose()
        {
            chatapp.Remove(this);

            chatapp.Broadcast(serializer.Serialize(new
            {
                type = MessageType.Leave,
                from = userId,
                userName = (String.IsNullOrEmpty(userName) ? userId : userName),
                message = $"Connection to {userId} has been terminated."
            }));
        }

        enum MessageType {
            Join,
            Broadcast,
            PickChatRequest,
            RequestPA,
            RequestTopUp,
            Leave,
            Logout,
            Handshake
        }

        class Payload
        {
            public MessageType Type { get; set; }
            public string Message { get; set; }
            public string From { get; set; }
            public string To { get; set; }
            public double TimeStamp { get; set; }
        }
    }
}