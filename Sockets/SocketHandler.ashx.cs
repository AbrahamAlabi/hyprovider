﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Web.WebSockets;

namespace HyProvider.Sockets
{
    /// <summary>
    /// Summary description for SocketHandler
    /// </summary>
    public class SocketHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            #region Hide
            //var user = "";
            //try
            //{
            //    user = context.Request.Cookies["username"].Value;
            //}
            //catch
            //{
            //}
            //if (context.IsWebSocketRequest)
            //{
            //    context.AcceptWebSocketRequest(new MicrosoftWebsockets());
            //} 
            #endregion

            var user = context.Request.QueryString["username"];
            string providerName = context.Request.QueryString["providerName"];
            if (context.IsWebSocketRequest)
            {
                context.AcceptWebSocketRequest(new ChatHandler(user, providerName));
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}