﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HyAdmin.Startup))]
namespace HyAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
