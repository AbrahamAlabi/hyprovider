﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HyAdmin.Models.ViewModels
{


    public class RetailCustomerInfo
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public string TransactionReference { get; set; }
        public string Error { get; set; }
        public double Amount { get; internal set; }
        public string HashString { get; internal set; }
        public string Name { get; internal set; }
        public int MemberId { get; internal set; }
        public string LegacyCode { get; internal set; }
        public int DependantId { get; internal set; }
        public bool PaymentApproved { get; internal set; }
        public string Message { get; internal set; }
        public string Redirect { get; internal set; }
        public string MacKey { get; internal set; }
        public int ItemId { get; internal set; }
        public int ProductId { get; internal set; }
        public string Date { get; internal set; }
        public double PlanPrice { get; internal set; }
        public double Convenience { get; internal set; }
        public string Guid { get; internal set; }
        public string sDescription { get; internal set; }
        public string sPaymentReference { get; internal set; }
        public string sResponse { get; internal set; }
        public double AmountSent { get; internal set; }
        public bool TransferConfirmed { get; internal set; }
        public string RegistrationDate { get; internal set; }
        public dynamic OtherName { get; internal set; }
        public dynamic FirstName { get; internal set; }
        public dynamic LastName { get; internal set; }
        public dynamic PaymentMethod { get; internal set; }
        public string UserId { get; internal set; }
    }
}