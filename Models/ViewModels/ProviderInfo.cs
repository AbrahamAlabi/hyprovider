﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HyProvider.Models.ViewModels
{
    public class ProviderInfo
    {
        public int ProviderId { get; set; }
        public int ProviderKey { get; set; }
        public string ProviderName { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public string TransitionCode { get; set; }
        public DateTime LastLogin { get; set; }
        public dynamic Id { get; internal set; }
    }
}