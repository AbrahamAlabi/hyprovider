//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HyProvider.Models.ExistingDBs
{
    using System;
    using System.Collections.Generic;
    
    public partial class AppSetting
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public Nullable<System.DateTime> EndTime { get; set; }
        public string Message { get; set; }
        public bool IsCurrent { get; set; }
        public System.DateTime DateAdded { get; set; }
    }
}
