﻿using System.Web;
using System.Web.Optimization;

namespace HyAdmin
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //        "~/Content/files/bower_components/jquery/js/jquery.min.js",
            //        "~/Content/files/bower_components/jquery/js/jquery-ui.min.js"
            //    ));
            

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/files/bower_components/bootstrap/css/bootstrap.min.css",
                      "~/Content/files/assets/pages/waves/css/waves.min.css",
                      "~/Content/files/assets/icon/feather/css/feather.css",
                      "~/Content/files/assets/css/font-awesome-n.min.css",
                      "~/Content/files/assets/css/style.css",
                      "~/Content/files/assets/css/widget.css"));
        }
    }
}
