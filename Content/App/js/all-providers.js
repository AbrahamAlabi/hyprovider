'use strict';
$(document).ready(function() {
    // statistics-chart start
    var chart = AmCharts.makeChart("onboarding-statistics-chart", {
        type: "serial",
        marginTop: 10,
        marginRight: 0,
        dataProvider: [{
            year: "Jan",
            value: 10
        }, {
            year: "Feb",
            value: 15
        }, {
            year: "Mar",
            value: 5
        }, {
            year: "Apr",
            value: 18
        }, {
            year: "May",
            value: 30
        }, {
            year: "Jun",
            value: 25
        }, {
            year: "July",
            value: 60
        }
        //    , {
        //    year: "Aug",
        //    value: 300
        //}, {
        //    year: "Sep",
        //    value: 200
        //}, {
        //    year: "Oct",
        //    value: 450
        //}, {
        //    year: "Nov",
        //    value: 360
        //}, {
        //    year: "Dec",
        //    value: 500
        //}
        ],
        valueAxes: [{
            axisAlpha: 0,
            dashLength: 6,
            gridAlpha: 0.1,
            position: "left"
        }],
        graphs: [{
            id: "g1",
            bullet: "round",
            bulletSize: 10,
            lineColor: "#33db9e",
            lineThickness: 3,
            negativeLineColor: "#33db9e",
            type: "smoothedLine",
            valueField: "value"
        }],
        chartCursor: {
            cursorAlpha: 0,
            valueLineEnabled: false,
            valueLineBalloonEnabled: true,
            valueLineAlpha: false,
            color: "#fff",
            cursorColor: "#333",
            fullWidth: true
        },
        categoryField: "year",
        categoryAxis: {
            gridAlpha: 0,
            axisAlpha: 0,
        }
    });
    // statistics-chart start
});

$('#provider-listing tr').click(function () {
    var $elem = $(this);
    console.log($elem)
});
