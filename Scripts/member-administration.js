﻿//"use strict";
var _ENVIRONMENT = 'production';

//==========================================================================================
// Loaders
var $memberLoading = $('.member-loading');
var $checkInLoading = $('.member-check-in-loading');
var $tariffLoading = $('.tariff-loading');
var $paLoading = $('.pa-search-loading');

//==========================================================================================
// Member Search Elements
var $memberSearch = $('.member-search');
var $memberSearchError = $('.member-search-error');
var $paSearchError = $('.pa-search-error');
var $viewMember = $('.view-member');
var $memberSearchResult = $('.member-search-result');
var $memberPrescenceConfirmation = $('.member-prescence-confirmation');
var $memberBasicInfo = $('.member-basic-info');
var $memberStatuses = $('.member-statuses');
var $memberStatuses1 = $('.member-statuses1');
var $memberBenefitLimits = $('.member-benefit-limits');
var $memberPostSearchDivs = $('.member-post-search');
var $memberSearchValue = $('.member-search-value');
var $allDiagnosis = $('#all-diagnosis');

//==========================================================================================
// form Validation
var form = $('#formUrl');
var token = $('input[name="__RequestVerificationToken"]', form).val();

//==========================================================================================
// URLs
var memberSearchURL = $('#member-search-url').data('url');
var memberEligibilityURL = $('#member-eligibility-url').data('url');
var memberCheckInURL = $('#member-check-in-url').data('url');
var memberIssuedPAsURL = $('#member-issued-pas-url').data('url');
var memberCoveredServicesURL = $('#member-covered-services-url').data('url');

var paSearchURL = $('#pa-search-url').data('url');
var procedureTariffURL = $('#procedure-tariff-url').data('url');
var noAuthAccessCheckURL = $('#noauth-access-check-url').data('url');
var checkInListURL = $('#check-in-list-url').data('url');

var diagnosisListURL = $('#diagnosis-list-url').data('url');
var procedureListURL = $('#procedure-list-url').data('url');
var AllProceduresURL = $('#all-procedure-list-url').data('url');
var PARulesTestWithTariffURL = $('#pa-rules-test-url').data('url');

var noAuthListURL = $('#no-auth-list-url').data('url');
var noAuthRemovalURL = $('#no-auth-removal-url').data('url');
var noAuthViewClaimURL = $('#no-auth-view-claim-url').data('url');

var paRequestURL = $('#pa-request-url').data('url');
var paIssueDetailsURL = $('#pa-issue-details-url').data('url');

var saveClaimURL = $('#save-claim-url').data('url');
var requestPAURL = $('#request-pa-url').data('url');

var paFetchDetailsURL = $('#fetch-pa-details-url').data('url');
var preAuthListURL = $('#pre-auth-list-url').data('url');
var preAuthViewClaimURL = $('#pre-auth-view-claim-url').data('url');
var preAuthRemovalURL = $('#pre-auth-removal-url').data('url');
var submitPAURL = $('#pre-auth-submission-url').data('url');

var exclusionListURL = $('#pre-auth-exc-list-url').data('url');
var submitExclusionPAURL = $('#exclusion-submission-url').data('url');

var resetPassword = $('#reset-password-url').data('url');

//==========================================================================================
// member Data
var MemberData;
var checkInData = [];

//==========================================================================================



LogEvent("Page Init", "Page begins loading at current time");



// NAVIGATION
$('.search-btn').click(function () {
    var $isInput = $(this).prev();
    var target = $isInput.val();
    if (target.trim() === '') {
        $isInput.focus();
        return;
    }

    $memberSearchValue.val(target);
    $memberSearch.click();
    GotoPage('member-administration');
});
$('.page-nav').click(function () {
    var id = $(this).data('id');
    GotoPage(id);
});
function GotoPage(id) {
    $('.page').addClass('hide');
    $('.page-name').text(jsUcfirst(id));

    if (id === 'no-auth') {
        id = 'billing';

        setTimeout(function () { ShowNoAuthIntro(); }, 1000);
        ParseCheckInData();
        setTimeout(function () { localStorage.setItem("intro", "skipped"); }, 3000);
        
    }

    if (id === 'pre-auth') {
        ResetPASubmitContent();
        if (PreAuthDataRetrived !== true)
            GetPreAuthData();
    }

    if (id === 'exclusions') {
        ResetPASubmitContent();
        if (ExclusionsDataRetrived !== true)
            GetExclusionData();
    }

    $('#' + id).removeClass('hide');
}

//==========================================================================================
// MEMBER ADMINISTRATION
var iter = 0;
$introNextBtn = $('body').find('.introjs-nextbutton').first();

$(function () {

    setTimeout(function () {
        GetFilteredDiagnosis();
    }, 1500);

    $('.prev-def').click(function (e) { e.preventDefault(); });
    try { $memberSearchValue.focus(); } catch (e) { LOG(e); }

    // =============Member Searh======================
    // Search for member by Name or enrollee number
    $memberSearch.click(function (e) {
        //const overallStartTime = performance.now();

        MemberData = undefined;
        $memberLoading.removeClass('hide');
        $('.member-check-in').addClass('hide');

        $memberStatuses.addClass('hide');
        $memberBasicInfo.addClass('hide');
        $memberPostSearchDivs.addClass('hide');
        $memberPrescenceConfirmation.removeClass('hide');

        var target = VMSI();
        if (target === false) {
            hideLoading();
            return false;
        }

        // Make Ajax Call Here
        var error = function (data) {
            hideLoading();
            ReturnError($memberSearchError, '', data);
        }
        var success = function (data) {
            data = window.JSON.parse(data);
            var res = '';
            var count = 0;
            for (var i = 0; i < data.length; i++) {
                var d = data[i];

                //if (d.isterminated === false) {
                var action = (d.isterminated === false && d.groupTerminated === 0 && d.memberTerminated === 0) ?
                    '<a id="' + d.memberid + '" class="btn btn-outline-primary waves-effect waves-light btn-sm view-member prev-def" onclick="ViewMember(\'' + d.memberid + '\')">View&nbsp;<i class="fa fa-eye"></i></a>'
                    : '<a class="btn btn-danger waves-effect waves-light btn-sm" onclick="NotifyDanger(\'Notification\', \'Member Record Inactive. You cannot procced from this point\')">&nbsp;<i class="fa fa-ban"></i>Stop</a>';

                count++;
                res += '<tr>' +
                    '<td style="width: 50px" class="text-left">' +
                    action +
                    '</td>' +
                    '<td>' +
                    '<div class="d-inline-block align-middle">' +
                    '<img src="https://online.hygeiahmo.com/hyprovider/content/images/default-avatar-sm.png" style="height: 40px;" alt="user image" class="img-radius  member-image img-40 align-top m-r-15">' +
                    '<div class="d-inline-block" style="height: 40px;">' +
                    '<h6>' + jsUcfirst(d.lastname) + ' ' + jsUcfirst(d.firstname) + ' ' + jsUcfirst(d.othername) + '</h6>' +
                    '<p class="text-muted sr-enrollee-no">' + d.legacycode + '/' + d.dependantid + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';
                LogEvent("Member search", "Searched for " + target);
            }
            ReturnSuccess($memberSearchError, 'Search result returned ' + count + ' results');
            $memberSearchResult.html(res);
            hideLoading();

            $('body').find('.introjs-donebutton').first().click();
            //ShowMemberSearchResultIntro();
        };
        var data = {
            searchTarget: target,
            __RequestVerificationToken: token
        };
        POSTToServer2(memberSearchURL, data, success, error);

        //const overallDuration = performance.now() - overallStartTime;
        //LOG('Member Search took ' + overallDuration + 'ms');
    });
    GetCheckInData($('#sdt').val(), $('#edt').val());
    SetMaxProcedureRequestDate();
});
function ResetValue() {
    SetMaxProcedureRequestDate();
}
function SetMaxProcedureRequestDate() {
    var today = $('#sdt').val();
    var tomorrow = $('#pdt').val();
    document.getElementById("procedure-date").setAttribute("min", today);
    document.getElementById("procedure-date").setAttribute("max", tomorrow);
}

function VMSI() {
    var target = $memberSearchValue.val().replace(/-/g, '').replace(/:/g, '').trim();
    if (target === '' || target.length > 50) {
        ReturnError($memberSearchError, "Please provide a valid search target");
        return false;
    }
    return target;
}
function ReturnSuccess(e, msg, error) {
    if (msg === '' || msg === undefined)
        msg = 'Operation successful.';

    e.html("<label class='label label-success'><i class='fa fa-check'></i> " + msg + "</label>");
    LOG(error);
}
function ViewMember(memberId) {
    $checkInLoading.removeClass('hide');

    // Show member-check-in card to give perception of no-delay
    setTimeout(function () { $('.member-check-in').removeClass('hide'); }, 1000);

    var error = function (data) {
        hideLoading();
        ReturnError1($memberSearchError, '', data);
    }
    var success = function (data) {
        MemberData = window.JSON.parse(data);
        //LOG(MemberData);

        // Set Member basic information
        SetMemberBasics(MemberData);

        // Show member image id available
        $('.member-image').prop('src', FormatPixPath(MemberData.eligibilityInfo.picture));

        $('html,body').animate({ scrollTop: $('.member-check-in').offset().top - 20 }, 230);
        ResetSearchValues();
        hideLoading();
        hideCheckInLoading();

        $('body').find('.introjs-donebutton').first().click();
        LogEvent("View member", "Viewing details for memberId: " + memberId);
    };
    var data = {
        memberId: memberId,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    POSTToServer2(memberEligibilityURL, data, success, error);
}
function SetMemberBenefits(data) {
    try {
        var benfitResult = "<table class='table table-striped table-hover'><tr><th>Benefit</th><th>Limit</th><th>Used</th><th>Balance</th></tr>";
        for (var i = 0; i < data.benefits.length; i++) {
            var xata = data.benefits[i];
            var balance = (parseFloat(xata.limit.replace(/,/g, '').replace('₦ ', '')) - parseFloat(xata.used.replace(/,/g, '').replace('₦ ', ''))).toFixed(2);
            benfitResult += "<tr><td>" + xata.benefit + "</td><td>" + xata.limit + "</td><td>" + xata.used + "</td><td>₦ " + Number(balance).toLocaleString('en') + "</td><tr/>";
        }
        benfitResult += "</table>";
        $memberBenefitLimits.html(benfitResult);
    } catch (e) {
        LOG(e);
    }
}
function SetMemberBasics(data) {
    $('.member-fullname').html("<h5>" + jsUcfirst(data.eligibilityInfo.lastName) + ' ' + jsUcfirst(data.eligibilityInfo.firstName) + ' ' + jsUcfirst(data.eligibilityInfo.othername) + "</h5>");
    $('.member-number').html("<span class='text-primary'>" + data.eligibilityInfo.legacyCode + '/' + data.eligibilityInfo.dependantId + "</span>");
    $('.member-plan').html(jsUcfirst(data.eligibilityInfo.plan));
    $('.member-hospital').html(jsUcfirst(data.eligibilityInfo.provider));
    if (data.isCapitated === true) {
        $('.member-capitation').html('<label class="label label-primary">True</label>');
    } else {
        $('.member-capitation').html('<label class="label label-default">False</label>');
    }
}
function SetMemberStatuses(data) {

    $memberStatuses.html('');
    $memberStatuses1.html('');
    $memberPostSearchDivs.addClass('hide');

    var sr = '<div class="form-radio">';

    if ((data.eligibilityInfo.status === 'ELIGIBLE')) {

        sr += '<div class="radio radio-matrial radio-success"><label class="text-success"><input type="radio"><i class="helper"></i>Enrollee is ACTIVE</label></div>';

        //data.eligibilityInfo.provider = "ROAMING";
        //if (data.eligibilityInfo.provider == "ROAMING")
        sr += '<div class="radio radio-matrial radio-success"><label class="text-success"><input type="radio"><i class="helper"></i>Active for Roaming</label></div>';
        if (data.hasAccessToProvider === true) {

            sr += '<div id="access-to-hospital" class="radio radio-matrial radio-success"><label class="text-success"><input type="radio"><i class="helper"></i>Access to ' + data.visitedProvider + ' allowed</div>';
            sr += '</div>';
            sr += '<div class="alert alert-success text-center" style="font-weight: bold;">This enrollee is allowed to use this hospital</div>';

            setTimeout(function () { GetissuedPAs(data); CheckNoAuthAccess(data, data.visitedProvider) }, 500);
            setTimeout(function () { GetBenefits(data.eligibilityInfo.planId); }, 500);
        }
        else {
            sr += '</div>';
            sr += '<div class="alert alert-danger text-center" style="font-weight: bold;">This enrollee is not allowed to use this hospital. Please contact Hygeia in case of emergencies and refferals</div>';
            setTimeout(function () { GetBenefits(data.eligibilityInfo.planId); }, 500);
        }
        $memberStatuses.append(sr);
        $memberStatuses1.html(sr);
    }
    $memberPostSearchDivs.removeClass('hide');

}
function SetDependants(data) {
    var relatives = "";
    if (data.relatives.length > 0)
        for (var i = 0; i < data.relatives.length; i++) {
            relatives += "<tr><td>"
                + jsUcfirst(data.relatives[i].lastName) + "</td><td>"
                + jsUcfirst(data.relatives[i].firstName) + "</td><td>"
                + jsUcfirst(data.relatives[i].otherName) + "</td><td>"
                + data.relatives[i].legacyCode + "/" + data.relatives[i].dependantId + "</td><td></tr>";
        }
    else
        relatives = "<tr><td colspan='4'>This enrollee has no registered dependants</td></tr>";
    $('.member-dependants').html(relatives);
}
function GetissuedPAs(data) {

    var error = function (d) {
        hideLoading();
        ReturnError1($memberSearchError, '', d);
    }
    var success = function (d) {
        d = window.JSON.parse(d);
        //LOG(d);
        var res = "";
        if (d.length > 0) {
            for (var i = 0; i < d.length; i++) {
                res += '<tr>' +
                    '<td class="text-left">' +
                    '<a id="' + d[i].paNumber + '" class="btn btn-info waves-effect waves-light btn-sm btn-smaller" onclick="ViewPADetails(\'' + d[i].paNumber + '\')" data-toggle="modal" data-target="#pa-lookup-modal">View</a>' +
                    '</td>' +
                    '<td>' + d[i].paNumber + '</td>' +
                    '<td>' + d[i].issueDate.replace('T', ' ') + '</td>' +
                    '</tr>';
            }
        } else {
            res += '<tr>' +
            '<td></td><td colspan="2" class="text-left">This enrollee has no PAs issued to your facility</td>' +
            '</tr>';
        }
        $('.member-pas').html(res);
    }
    var postData = {
        legacyCode: MemberData.eligibilityInfo.legacyCode,
        dependantId: MemberData.eligibilityInfo.dependantId,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    }
    //LOG(postData);
    POSTToServer2(memberIssuedPAsURL, postData, success, error);
}
function CheckNoAuthAccess(data, visitedProvider) {
    var error = function (data) {
        //LOG(data);
        //ReturnError1($memberSearchError, '', data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        //LOG(data);
        //data.hasNoAuthAccess = false;
        if (data.hasNoAuthAccess !== true) {
            $('#access-to-hospital').replaceWith('<div id="access-to-hospital" class="radio radio-matrial radio-warning"><label class="text-warning"><input type="radio"><i class="helper"></i>Access to ' + visitedProvider + ' allowed for <strong>SECONDARY CARE ONLY</strong></div>');
            //$('#access-to-hospital').replaceWith('alert-success').addClass('alert-warning').html("<strong>Note: </strong> Enrollee is allowed access at Lagoon Hospitals for secondary care only. Referral from primary provider required");
        }
        $checkInLoading.addClass('hide');
    }
    var datar = {
        legacyCode: MemberData.eligibilityInfo.legacyCode,
        dependantId: MemberData.eligibilityInfo.dependantId,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    }
    $checkInLoading.removeClass('hide');
    POSTToServer2(noAuthAccessCheckURL, datar, success, error);
}
function FormatPixPath(data) {
    try {
        var img = data.replace(/\\/g, "/").split("/");
        return "https://online.hygeiahmo.com/hydirect/uploads/pictures/" + img[img.length - 1];
    }
    catch (e) {
        return "https://online.hygeiahmo.com/hyprovider/content/images/default-avatar-sm.png";
    }
}
function ResetSearchValues() {
    var res = '<tr>' +
    '<td>' +
    '<div class="d-inline-block align-middle">' +
    '<img src="https://online.hygeiahmo.com/hyprovider/content/images/default-avatar-sm.png" alt="user image" class="img-radius img-40 align-top m-r-15 member-image">' +
    '<div class="d-inline-block" style="height: 40px;">' +
    '<h6>Member Name Appears Here</h6>' +
    '<p class="text-muted sr-enrollee-no">Enrollee Number Appears Here</p>' +
    '</div>' +
    '</div>' +
    '</td>' +
    '<td class="text-right">' +
    '<a disabled="disabled" class="btn btn-primary  btn-disabled waves-effect waves-light btn-sm prev-def">Action</a>' +
    '</td>' +
    '</tr>';
    $memberSearchResult.html(res);
    $memberSearchError.html('');
}
function ViewPADetails(paNumber) {
    $('.pa-code-search-value').val(paNumber);
    $('.pa-code-search').click();
};
function GetBenefits(planId) {

    var error = function (data) {
        //LOG(data);
        //ReturnError1($memberSearchError, '', data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(data); }
        setTimeout(function () {
            GetItems(data);
        }, 500);
    }
    var data = {
        planId: planId,
        __RequestVerificationToken: token
    }
    $memberLoading.removeClass('hide');
    POSTToServer2(memberCoveredServicesURL, data, success, error);
}
function GetItems(data) {
    var res = "";
    var iteration = iter;
    var lastItem = (((iter + 100) > data.length) ? ((iter + 50) - data.length) : data.length);
    try {
        for (var i = iter; i < lastItem; i++) {

            res += "<tr class='trs'><td>" + (i + 1) + "</td><td>" + data[i].proceduredesc + "</td><td>" + data[i].procedurecode +
            "</td><td><a href='javascript:void(0);' onclick='GetTariff(this, \"" + data[i].procedurecode + "\", \"" + data[i].planid + "\")'>Show Tariff</a></td></tr>";
            iteration = i;
        }
        $('#benefit-listing').append(res);
        if (lastItem === 50 && res !== "") {
            setTimeout(function () {
                GetItems(data);
            }, 500);
        }
        else {
            $('#benefit-listing').parent().DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5', 'csvHtml5'
                ]
            });
            $memberLoading.addClass('hide');
        }
    }
    catch (e) {
        //LOG(e);
    }

    iter = iteration;
}
$('.check-in-enrollee').click(function () {
    var data = {
        legacyCode: MemberData.eligibilityInfo.legacyCode,
        dependantId: MemberData.eligibilityInfo.dependantId,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    var error = function (data) {
        hideCheckInLoading();
        ReturnError1($memberSearchError, '', data);
    };
    var success = function (data) {
        LOG(data);
        
        GetCheckInData($('#sdt').val(), $('#edt').val());
        LogEvent("Member Check-In", "Checking in : " + MemberData.eligibilityInfo.legacyCode + "/" + MemberData.eligibilityInfo.dependantId);
    };
    POSTToServer2(memberCheckInURL, data, success, error);
    hideCheckInLoading();
});
$('.show-details').click(function () {

    if (MemberData === undefined)
        return;

    $memberPrescenceConfirmation.addClass('hide');
    $memberBasicInfo.removeClass('hide');
    $memberStatuses.removeClass('hide');

    // Set member benefits
    SetMemberBenefits(MemberData);

    // Show relatives
    SetDependants(MemberData);

    // Set Member statuses and labels
    SetMemberStatuses(MemberData);

    ShowCheckInDetailsIntro();
});
function GetTariff(e, procedureCode, planId) {
    var error = function (data) {
        //LOG(data);
        //ReturnError1($memberSearchError, '', data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        //LOG(data);
        $(e).text((data.tariff === null) ? "---" : data.tariff);
        $tariffLoading.addClass('hide');
        LogEvent("Tariff Check", "Clicked 'Show Tariff' button for procedure code: " + procedureCode);
    };
    var data = {
        memberId: MemberData.eligibilityInfo.memberId,
        providerId: __ProviderId,
        procedureCode: procedureCode,
        //planId: planId,
        __RequestVerificationToken: token
    };
    POSTToServer2(procedureTariffURL, data, success, error);
}
$("#tab1").click(function () {
    $(this).parent().addClass('active');
    $('#tab2').parent().removeClass('active');
});
$("#tab2").click(function () {
    $(this).parent().addClass('active');
    $('#tab1').parent().removeClass('active');
});

//var take = 10000;
var selectedDiagnosis = '';
var diagnosisElement;
var setCount = 0;
var selectedMember = {
    name: '',
    legacyCode: 0,
    dependantId: 0,
    encounterDate: ''
};
var introInitiated = false;

function GetFilteredDiagnosis(skip) {
    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        data = data.diagnosis;

        var diagnosis = "";
        for (var d = 0; d < data.length; d++) {
            diagnosis += "<tr><td><a href='javascript:void(0);' class='text-primary' onclick='SelectDiagnosis(\"" + data[d].diagnosisCode + "\", \""
                + data[d].diagnosisDesc + "\", this)'>Select</a></td><td>" + data[d].diagnosisDesc + "</td><td>" + data[d].diagnosisCode +
                "</td></tr>";
        };

        $('#diagnosis-list').html(diagnosis);
        $('.diagnosis-loading').addClass('hide');
        $('#diagnosis-content').DataTable();

        // Get No-auth Details
        setTimeout(function () {
            GetNoAuthData(true);
        }, 500);
    };
    var data = {
        __RequestVerificationToken: token
    }
    $('.diagnosis-loading').removeClass('hide');
    POSTToServer2(diagnosisListURL, data, success, error);
}
function GetNoAuthData(loadProcedures) {
    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        var noAuthList = data.claims;
        var noAuthSummary = data.summary[0];

        if (data.summary.length > 0) {
            $('#no-auth-total-sum').text(FormatMoney(noAuthSummary.totalCharge));
            $('#no-auth-approved-sum').text(FormatMoney(noAuthSummary.approved));
            $('#no-auth-denied-sum').text(FormatMoney(noAuthSummary.denied));
            $('#no-auth-discounted-sum').text(FormatMoney(noAuthSummary.discounted));

            var claimLines = "";
            for (var i = 0; i < noAuthList.length; i++) {
                claimLines += "<tr>" +
                    "<td>" + jsUcfirst(noAuthList[i].fullName) + "</td>" +
                    "<td>" + FormatMoney(noAuthList[i].totalCharge) + "</td>" +
                    "<td>" + FormatMoney(noAuthList[i].approved) + "</td>" +
                    "<td>" + FormatMoney(noAuthList[i].denied) + "</td>" +
                    "<td>" + ((noAuthList[i].initialStatus === 1 && noAuthList[i].finalStatus) ? '<label class="label label-success">True</label>' : '<label class="label label-danger">False</label>') + "</td>" +
                    "<td class='hide'>" + FormatDate(noAuthList[i].encouterDate) + "</td>" +
                    "<td class='hide'>" + noAuthList[i].procedureCode + "</td>" +
                    "<td class='hide'>" + jsUcfirst(noAuthList[i].procedureDesc) + "</td>" +
                    "<td class='hide'>" + noAuthList[i].legaCycode + "</td>" +
                    "<td class='hide'>" + noAuthList[i].dependantId + "</td>" +
                    '<td>' +
                    '<a class="btn btn-outline-danger waves-effect waves-light btn-sm btn-smaller" style="font-size: 12px;" onclick="RemovePCClaim(this, \'' + noAuthList[i].pcClaimItemId + '\')">Remove</a>&nbsp;' +
                    '<a class="btn btn-outline-secondary waves-effect waves-light btn-sm btn-smaller" style="font-size: 12px;" onclick="ViewPCClaimDetails(this, \'' + noAuthList[i].pcClaimItemId + '\')" data-toggle="modal" data-target="#pc-claim-modal">More</a></td>' +
                    "<td class='hide'>" + noAuthList[i].diagnosisCode + "</td>" +
                    "<td class='hide'>" + jsUcfirst(noAuthList[i].diagnosisDesc) + "</td>" +
                    "<td class='hide'>" + noAuthList[i].pcClaimItemId + "</td>" +
                    "<td class='hide'>" + FormatDate(noAuthList[i].dateAdded) + "</td>" +
                    "</tr>";
            }
            $('#no-auth-list tbody').html(claimLines);

            if (!$.fn.DataTable.isDataTable('#no-auth-list')) {
                $('#no-auth-list').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5', 'csvHtml5'
                    ],
                    "order": [[14, "desc"]]
                });
            }
        }
        else {
            $('#no-auth-total-sum').text(FormatMoney(0));
            $('#no-auth-approved-sum').text(FormatMoney(0));
            $('#no-auth-denied-sum').text(FormatMoney(0));
            $('#no-auth-discounted-sum').text(FormatMoney(0));
            $('#no-auth-list tbody').html("<tr><td colspan='15'>No ubsubmitted claims available</td></tr>");
        }

        $('.pre-auth-list-loading').addClass('hide');

        // Load procedures
        if (loadProcedures === true) {
            setTimeout(function () {
                LoadProcedures();
            }, 500);
        }
    };
    var data = {
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.pre-auth-list-loading').removeClass('hide');
    POSTToServer2(noAuthListURL, data, success, error);
}
var currentPCClaim;
function ViewPCClaimDetails(e, pcClaimItemId) {
    currentPCClaim = e;
   
    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        LOG(data);

        var $depId = $(currentPCClaim).parent().prev();
        var $legCode = $depId.prev();
        var $procDesc = $legCode.prev();
        var $procCode = $procDesc.prev();
        var $encDate = $procCode.prev();
        var $name = $encDate.prev().prev().prev().prev().prev();
        var $diagCode = $(currentPCClaim).parent().next();
        var $diagDesc = $diagCode.next();

        $('.claim-enrollee-name').text($name.text());
        $('.claim-enrollee-number').text($legCode.text() + "/" + $depId.text());
        $('.claim-encounter-date').text($encDate.text());
        $('.claim-diagnosis').text($diagDesc.text() + " // " + $diagCode.text());
        $('.claim-procedures').text($procDesc.text() + " // " + $procCode.text());
        $('#claim-panumber').text('');

        var procedureDiagnosis = data.details;
        var diag = ""; var proc = "";

        for (var i = 0; i < procedureDiagnosis.length; i++) {
            if ($diagDesc.text().indexOf(procedureDiagnosis.diagnosisCode) === -1)
                diag += "<br />" + jsUcfirst(procedureDiagnosis.diagnosisDesc) + " // " + procedureDiagnosis.diagnosisCode

            if ($procDesc.text().indexOf(procedureDiagnosis.procedureCode) === -1)
                proc += "<br />" + jsUcfirst(procedureDiagnosis.procedureDesc) + " // " + procedureDiagnosis.procedureCode
        }
        $('.claim-diagnosis').append(diag);
        $('.claim-procedures').append(proc);

        $('#denial-reasons').html(data.denialReasons);
        $('.no-auth-list-loading').addClass('hide');

        currentPCClaim = undefined;
    };
    var data = {
        pcClaimItemId: pcClaimItemId,
        __RequestVerificationToken: token
    };
    $('.no-auth-list-loading').removeClass('hide');
    POSTToServer2(noAuthViewClaimURL, data, success, error);
}
function RemovePCClaim(e, pcClaimItemId) {

    if (!confirm('Are you sure?'))
        return false;

    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        if (data.success === true) {
            NotifySuccess('Notification', 'Claim was removed successfully.');
            $('#no-auth-list').DataTable().destroy();
            $('#no-auth-list tbody').html('');
            GetNoAuthData(false);
        }
        else {
            NotifyDanger('Notification', 'Unable to remove claim.');
            LOG(data.error);
        }

        //currentPCClaim = undefined;
        $('.no-auth-list-loading').addClass('hide');
    };
    var data = {
        pcClaimItemId: pcClaimItemId,
        __RequestVerificationToken: token
    };
    $('.no-auth-list-loading').removeClass('hide');
    POSTToServer2(noAuthRemovalURL, data, success, error);
}

function SelectDiagnosis(diagnosisCode, diagnosisDesc, e) {

    var selectedName = $('.check-in-fullname').text();
    if (selectedName === '') {
        NotifyDanger('Notification', 'Please select a member on the left pane to begin');
        return false;
    }

    var added = false;
    $('.selected-disgnosis').each(function () {
        if ($(this).text() === diagnosisCode) {
            NotifyDanger('Notification', 'This diagnosis has been added previously');
            added = true;
        }
    });
    if (added === true) return false;

    $('#temp-no-auth-diagnosis-list').append("<table style='width: 100%'><tr>" +
        "<td style='width: 100px;' class='selected-disgnosis'>" + diagnosisCode + "</td>" + 
        "<td>" + diagnosisDesc + "</td>" +
        "<td style='width: 40px;' class='text-right'><a href='javascript:void(0);' class='remove-diagnosis text-danger'><i class='fa fa-trash'></i></a></td>" +
        "</tr></table>");
    $("#temp-no-auth-diagnosis-list").scrollTop($("#temp-no-auth-diagnosis-list")[0].scrollHeight);
}
$('#temp-no-auth-diagnosis-list, #no-auth-params').on('click', '.remove-diagnosis', function () {
    $(this).parent().parent().remove();
});
$('#temp-request-diagnosis-list, #request-params').on('click', '.remove-diagnosis', function () {
    $(this).parent().parent().remove();
});
$('#request-params').on('click', '.remove-diagnosis', function () {
    $(this).parent().parent().remove();
});
$('#pa-request-diagnosis-list').on('click', '.remove-diagnosis', function () {
    $(this).parent().parent().remove();
});
$("#add-temp-diagnosis").click(function () {
    var content = $('#temp-no-auth-diagnosis-list').html().trim();
    if (content === "" || content.length === 0) {
        NotifyDanger('Notification', 'You did not add any diagnosis');
        return false;
    }

    if (noAuthType === 'submit-no-auth') {
        $("#no-auth-diagnosis-list").append(content);
    }
    else {
        $("#pa-request-diagnosis-list").append(content);
    }
    $('#temp-no-auth-diagnosis-list').html('');
});


var procedureIsSelecting = false;
function SelectProcedure(procedureCode, procedureDesc, e) {

    var selectedName = $('.check-in-fullname').text();
    if (selectedName === '') {
        NotifyDanger('Notification', 'Please select a member on the left pane to begin');
        return false;
    }

    if (procedureIsSelecting === true) {
        NotifyWarning('Notification', 'Please wait until current operation completes');
        setTimeout(function () { procedureIsSelecting = false; }, 1000);
        return false;
    }

    var added = false;
    $('.selected-procedurecode').each(function () {
        if ($(this).text() === procedureCode) {
            NotifyDanger('Notification', 'This procedure has been added previously');
            added = true;
        }
    });
    if (added === true) return false;

    var diagnosisCode = $('.selected-disgnosis').first().text().trim();
    if (diagnosisCode === '') {
        NotifyDanger('Notification', 'Please add at least one diagnosis diagnosis');
        return false;
    }

    procedureIsSelecting = true;
    
    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        procedureIsSelecting = false;

        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        var limit = data.limit;
        console.log(data);

        try {
            var tariff = parseInt(data.tariff.tariff);

            var options = "";
            for (var ix = 0; ix < limit; ix++) {
                if (ix === 0)
                    options += "<option selected='selected'>" + (ix + 1) + "</option>";
                else
                    options += "<option>" + (ix + 1) + "</option>";
            }
            var readonly = ((tariff === undefined || isNaN(tariff) || tariff === 0) ? "" : "readonly='readonly' ");
            var value = "value='" + ((tariff === undefined || isNaN(tariff) || tariff === 0) ? '' : tariff) + "'";
            if (procedureCode === "90275" || procedureCode === "V2020") {
            value = "value='' ";
            readonly = "";
        }

            var res =
                "<tr>" +
                "<td style='width: 60%;'>" + jsUcfirst(procedureDesc.replace(/&sq;/g, '\'').replace(/&dq;/g, '"')) + "</td>" +
                "<td class='selected-procedurecode'>" + procedureCode + "</td>" +
                "<td><input style='width: 78px;padding: 5px;height: 24px;' class='form-control' " + value + readonly + " /></td>" +
                "<td><select class='form-control' style='width: 60px;padding: 5px;height: 28px;margin-top: -4px;'>" + options + "</select></td>";

            if (data.approved !== undefined) {

                if (data.approved === true) {
                    res += "<td><label class='label label-success'><i class='fa fa-check'></i></label>&nbsp;" +
                        "<a class='text-c-red' onclick='RemoveProcedureSet(this);' href='javascript:void(0);'><i class='fa fa-trash'></i></a>" +
                        "</td>";
                }
                else {
                    var reasons = '';
                    for (var i = 1; i < data.denialReasons.length - 1; i++) {
                        if (i === 1)
                            reasons += "<div style='margin-top: -10px;'><strong>" + i + ". </strong>" + data.denialReasons[i] + "</div>";
                        else
                            reasons += "<br /><div style='margin-top: -10px;'><strong>" + i + ". </strong>" + data.denialReasons[i] + "</div>";
                    }
                    res += "<td>" +
                        "<a onclick='InAppDenialReasons(this);' href='javascript:void(0);'><label class='label label-danger' style='cursor: pointer;'><i class='fa fa-ban'></i>&nbsp;Denial Reasons</label></a>&nbsp;" +
                        "<div class='hide'>" + reasons + "</div>" +
                        "<a class='text-c-red' onclick='RemoveProcedureSet(this);' href='javascript:void(0);'><i class='fa fa-trash'></i></a>" +
                        "</td>";
                }
            }
            else {
                res += "<td><a class='text-c-red' onclick='RemoveProcedureSet(this);' href='javascript:void(0);'><i class='fa fa-trash'></i></a></td>";
            }
            res += "</tr>";
        } catch (e) { console.log(e); }

        $('#select-procedures').append(res);
        $('#add-set').removeClass('hide');
        $('.procedure-loading').addClass('hide');
    };
    var data = {
        providerId: __ProviderId,
        legacyCode: selectedMember.legacyCode,
        dependantId: selectedMember.dependantId,
        procedureCode: procedureCode,
        isNoAuthRequest: true,
        isPARequest: (noAuthType !== 'submit-no-auth'),
        diagnosisCode: diagnosisCode,
        __RequestVerificationToken: token
    };
    $('.procedure-loading').removeClass('hide');
    POSTToServer2(PARulesTestWithTariffURL, data, success, error);
}
$("#add-temp-procedure").click(function () {
    var content = $('#select-procedures').html().trim();
    if (content === "" || content.length === 0) {
        NotifyDanger('Notification', 'You did not add any procedure');
        return false;
    }

    if (noAuthType === 'submit-no-auth') { $("#selected-procedures").append(content); } else { $("#selected-pa-procedures").append(content); }
    $('#select-procedures').html('');
});

$('.save-claim').click(function () {

    var allDiagnosis = [];
    var allProcedures = [];
    var hasErrors = false;
  
    $('.selected-disgnosis').each(function () {
        allDiagnosis.push($(this).text());
    });
    $('.selected-procedurecode').each(function () {
        var $procedure = $(this);
        var $amount = $procedure.next();
        var $qty = $amount.next();

        var amount = parseFloat($amount.children().first().val());
        hasErrors = amount === 0 || isNaN(amount);
        allProcedures.push(
            $procedure.text() +
            "#ps#" + amount +
            "#ps#" + parseInt($qty.children().first().val())
        );
    });
    if (hasErrors === true) {
        NotifyDanger('Notification', 'Please provide an amount for each item');
        return;
    }
    if (allDiagnosis.length === 0) {
        NotifyDanger("Notification", "Please at least one diagnosis");
        return;
    }
    if (allProcedures.length === 0) {
        NotifyDanger("Notification", "Please at least one procedure");
        return;
    }

    var allItems = allDiagnosis.join() + "#cs#" + allProcedures.join();

    var encounterDate = $('.check-in-encounter-date').first().text();
    var physicianId = $('#mdcn-number').val().trim();
    if (physicianId === '') {
        NotifyDanger("Notification", "Please provide MDCN number");
        return;
    }

    var enrolleeSigned = $('#enrollee-signed').prop("checked");
    var d = (($('.check-in-fullname').text().split('('))[1].split(')'))[0];
    var enrollee = d.split('/');

    $('.save-claim').addClass('hide');

    var data = {
        legacyCode: enrollee[0],
        dependantId: enrollee[1],
        providerId: __ProviderId,
        encounterDate: encounterDate,
        claim: allItems,
        physicianId: physicianId,
        enrolleeSigned: enrolleeSigned,
        __RequestVerificationToken: token
    };
    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        $('.save-claim').removeClass('hide');
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        if (data.result.length > 0 && data.errors.length === 0) {
            NotifySuccess("Notification", "All items in this set were processed without errors");
        }
        else if (data.result.length > 0 && data.errors.length > 0) {
            var errors = "";
            for (var i = 0; i < data.errors.length; i++) {
                errors += "<br />" + data.errors[i];
            }
            NotifyWarning("Notification", data.result.length + " item(s) in this set processed without errors.<br />" + data.errors.length + " item(s) had errors." + errors);
        }
        else if (data.result.length === 0 && data.errors.length > 0) {
            var error = "";
            for (var j = 0; j < data.errors.length; j++) {
                error += "<br /><br />" + data.errors[i];
            }
            NotifyDanger("Notification", "All items had errors." + error);
        }

        $('#selected-procedures').html('');
        $('#no-auth-diagnosis-list').html('');
        GetNoAuthData(false);
    };
    $('#check-in-action-loading').removeClass('hide');
    POSTToServer2(saveClaimURL, data, success, error);
});

function InAppDenialReasons(e) {
    NotifyDanger('Denial Reasons', $(e).next().html());
}
function RemoveProcedureSet(e) {
    $(e).parent().parent().remove();
    if ($('#selected-procedures').text().trim() === "") {
        $('#add-set').addClass('hide');
        $('#selected-procedures').html("<tr><td colspan='5'>No procedures selected</td></tr>");
    }
}
function LoadProcedures() {

    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        var procedures = "";
        for (var d = 0; d < data.length; d++) {
            procedures += "<tr><td><a href='javascript:void(0);' class='text-primary' onclick='SelectProcedure(\"" + data[d].procedureCode + "\", \""
                + data[d].procedureDesc.replace(/'/g, '&sq;').replace(/"/g, '&dq;').replace(/\n/g, '') + "\", this)'>Select</a></td><td>" + data[d].procedureCode + "</td><td>" + data[d].procedureDesc +
                "</td></tr>";
        };

        $('#procedure-list').html(procedures);
        $('.procedure-loading').addClass('hide');
        $('#procedure-content').DataTable({
            "order": [[2, "desc"]]
        });

        setTimeout(function () {
            LoadPARequests();
        }, 500);
    }
    var data = {
        __RequestVerificationToken: token
    }
    $('.procedure-loading').removeClass('hide');
    POSTToServer2(AllProceduresURL, data, success, error);
}

//==========================================================================================
// OTHERS
function hideLoading() {
    setTimeout(function () {
        $memberLoading.addClass('hide');
    }, 500);
}
function hideCheckInLoading() {
    setTimeout(function () {
        $checkInLoading.addClass('hide');
    }, 500);
}
function jsUcfirst(string) {
    try {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    } catch (e) { return ''; }
}
function LOG(msg) {
    if (_ENVIRONMENT === 'development')
        console.log(msg);
}
function ReturnError(e, msg) {
    e.html("<label class='label label-danger'><i class='fa fa-ban'></i> " + msg + "</label>");
}
function ReturnError1(e, msg, error) {
    if (msg === '')
        msg = 'Operation failed. A network issue may have occured';

    e.html("<label class='label label-danger'><i class='fa fa-ban'></i> " + msg + "</label>");
    if (_ENVIRONMENT === 'development')
        LOG(error);
}
function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (window.JSON.stringify(list[i]) === window.JSON.stringify(obj)) {
            return true;
        }
    }

    return false;
}
function GenerateDate(inc) {
    //var today = new Date();
    //var dd = today.getDate();
    //var mm = today.getMonth() + 1;
    //var yyyy = today.getFullYear();

    //if (dd < 10) dd = '0' + dd;
    //if (mm < 10) mm = '0' + mm;

    //var date = yyyy + '-' + mm + '-' + (parseInt(dd) + inc);

    var date = ""
    return date;
}
function FormatDate(date) {
    try { return (date.split('T'))[0] } catch (e) { return date; }
}
function addDays(date, days) {
    var result = new Date(date);
    var newer = new Date(result.setDate(result.getDate() + days));
    return newer.getFullYear() + '-' + (newer.getMonth() + 1) + '-' + (((newer.getDate() + '').length === 1) ? ('0' + newer.getDate()) : newer.getDate());
}
function FormatMoney(n) {
    try { n = parseFloat(n).toFixed(2);
        return '₦ ' + Number(n).toLocaleString('en');
    }
    catch (e) {
        return n;
    }
}

//==========================================================================================
// PA Look Up
$('.pa-code-search').click(function () {
    $('.pa-search-enrollee-name').text('');
    $('.pa-search-enrollee-number').text('');
    $('.pa-search-amount-granted').text('');
    $('.pa-search-diagnosis').text('');
    $('.pa-search-provider').text('');
    $('.pa-search-procedures').text('');

    var paNumber = $('.pa-code-search-value').val();

    if (paNumber.trim() === "") {
        NotifyDanger('Notification', 'Please supply a PA number');
        return;
    }

    var error = function (data) {
        //LOG(data);
        $paLoading.addClass('hide');
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        //LOG(data);

        if (data.length > 0) {
            $('.pa-search-enrollee-name').text(jsUcfirst(data[0].enrolleelastname) + ' ' + jsUcfirst(data[0].enrolleefirstname) + ' ' + jsUcfirst(data[0].enrolleemiddlename));
            $('.pa-search-enrollee-number').text(data[0].iid + '/' + data[0].dependantnumber);
            $('.pa-search-amount-granted').text(FormatMoney(data[0].amountgranted));
            $('.pa-search-diagnosis').text(data[0].diagnosisDesc + '(' + data[0].diagnosisCode + ')');
            $('.pa-search-provider').text(data[0].providername);
            var res = '<table class="table table-striped"><tr><th>Procedure</th><th>Code</th><th>Amount</th></tr>';
            for (var i = 0; i < data.length; i++) {
                res += '<tr><td>' + jsUcfirst(data[i].procedureDesc) + '</td><td>' + data[i].procedureCode + '</td><td>' + FormatMoney(data[i].procedureValue) + '</td></tr>';
            }
            $('.pa-search-procedures').html(res);
            $paSearchError.html("<label class='label label-success'><i class='fa fa-check'></i> Search was successful</label>");
        }
        else {
            $paSearchError.html("<label class='label label-danger'><i class='fa fa-ban'></i> PA code was not found</label>");
        }
        $paLoading.addClass('hide');
    }
    var data = {
        paNumber: paNumber,
        __RequestVerificationToken: token
    }
    $paLoading.removeClass('hide');
    POSTToServer2(paSearchURL, data, success, error);
});

$('.pull-check-in-data').click(function () {
    $('#dt-header').html('');
    var startDate = $('#check-in-start-date').val();
    var endDate = $('#check-in-end-date').val();
    if (startDate.trim() === '') {
        NotifyDanger('Notification', 'Please provide a date to pull check-in data from');
        return false;
    }
    if (endDate.trim() === '') {
        NotifyDanger('Notification', 'Please provide an end date');
        return false;
    }
    $('#check-in-list').DataTable().destroy();
    GetCheckInData(startDate, endDate);
});
function GetCheckInData(start, end) {
    $('#check-in-start').text(start);
    $('#check-in-end').text(end);
    var error = function (data) {
        console.log(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        if (data.error !== null && data.error !== undefined) {
            NotifyDanger("Notification", data.error);
            $('.check-in-loading').addClass('hide');
            return false;
        }

        checkInData = data;
        ParseCheckInData();
        $('.check-in-loading').addClass('hide');
    };
    var data = {
        startDate: start,
        endDate: end,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.check-in-list').html('');
    $('.check-in-loading').removeClass('hide');
    POSTToServer2(checkInListURL, data, success, error);
}
function ParseCheckInData() {
    var res = "";
    for (var i = 0; i < checkInData.length; i++) {
        res += "<tr>" +
            "<td><a class='btn btn-outline-secondary btn-block btn-sm submit-no-auth'>Submit No-Auth</a></td>" +
            "<td><a class='btn btn-outline-secondary btn-block btn-sm request-pa'>Request PA</a></td>" +
            "<td style='padding-top: 15px !important;' title='" + jsUcfirst(checkInData[i].lastName) + " " + jsUcfirst(checkInData[i].firstName) + " " + jsUcfirst(checkInData[i].lastName)
            + "'><span style='display: inline-block;width: 120px;overflow: hidden;'>" + jsUcfirst(checkInData[i].lastName) + " " + jsUcfirst(checkInData[i].firstName) + "</span></td>" +
            "<td style='padding-top: 15px !important;'>" + checkInData[i].legacyCode + "/" + checkInData[i].dependantId + "</td>" +
            "<td style='padding-top: 15px !important;'>" + FormatDate(checkInData[i].dateAdded) + "</td>" +
            "</tr>";
    }
    
    $('.check-in-list').html(res);
    if (checkInData.length > 0) {
        setTimeout(function () {
            if (!$.fn.DataTable.isDataTable('#check-in-list')) {
                $('#check-in-list').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5', 'csvHtml5'
                    ],
                    "order": [[4, "desc"]]
                });
            }
        }, 500);

        $("#empty-content").html("<h3>Select a task from the left to <strong>Submit No-Auth</strong> or to <strong>Request PA</strong></h3>" +
            "<p class='text-center alert alert-info'>The details of the enrollee selected will show up on the right.<br />Then you can <strong>Add Procedure Set</strong></p>");
    }
    else {
        $("#empty-content").html("<h3 class='text-center'>There are no checked-in enrollees listed at the moment</h3>" +
            "<p class='text-center alert alert-info'>Use the form on the top left to retrieve check -in records to begin.</p>");
    }
}

var noAuthType;
var paRequestedBefore = false; // To figure when to destroy datatable and re-create
$('#check-in-datatable').on('click', '.submit-no-auth', function () {
    setCount = 0;
    noAuthType = 'submit-no-auth';

    // reset content of diagnosis selection for both PA and No-Auth requests
    $('#temp-no-auth-diagnosis-list').html('');
    $('#no-auth-diagnosis-list').html('');
    $('#temp-pa-request-diagnosis-list').html('');
    $('#pa-request-diagnosis-list').html('');

    // reset content of procedure selection for both PA and No-Auth requests
    $('#selected-procedures').html('');
    $('#select-procedures').html('');
    

    $('#selected-procedures').html('');
    $('.m-d-back').click();

    var $e = $(this);
    var enrolleeNumber = $e.parent().next().next().next().text().split('/');
    selectedMember.name = $e.parent().next().next().text();
    selectedMember.encounterDate = $e.parent().next().next().next().next().text();

    selectedMember.legacyCode = enrolleeNumber[0];
    selectedMember.dependantId = enrolleeNumber[1];

    try {
        $('.check-in-fullname').html(selectedMember.name + " (" + selectedMember.legacyCode + "/" + selectedMember.dependantId + ")");
        $('.check-in-encounter-date').html(selectedMember.encounterDate);
        $('.pa-requester-container').addClass('hide');
        $('.empty-container').addClass('hide');
        $('.no-auth-submission-container').removeClass('hide');

        // Hide the other procedure table
        $('#f-p-c').addClass('hide');

        // Show All Here
        $('#a-p-c').removeClass('hide');

        $('#no-auth-params').html(
            '<div>' +
                '<div style="border: 1px solid #ccc;background: whitesmoke;padding: 15px;max-height: 60px;">' +
                    '<h4 class="pull-left">Diagnosis <small> - Add one or more diagnosis here</small></h4>' +
                    '<a class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#diagnosis-selection" style="top: -36px;position: relative;">Add&nbsp;<i class="fa fa-plus"></i></a>' +
                    '<br style="clear: both;">' +
                '</div>' +
                '<div style="border: 1px solid #ccc;border-top: 0;padding: 15px;">' +
                    '<ul id="no-auth-diagnosis-list"></ul>' +
                '</div><br>' +

                '<div style="border: 1px solid #ccc;background: whitesmoke;padding: 15px;max-height: 60px;">' +
                    '<h4 class="pull-left">Procedure <small> - Add one or more procedure here</small></h4>' +
                    '<a class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#procedure-selection" style="top: -36px;position: relative;">Add&nbsp;<i class="fa fa-plus"></i></a>' +
                    '<br style="clear: both;">' +
                '</div>' +
                '<div style="border: 1px solid #ccc;border-top: 0;padding: 15px;">' +
                    '<div id="no-auth-procedure-list">' +
                        '<table class="table">' +
                            '<thead>' +
                                '<tr><th>Procedure</th><th>Code</th><th>Amount</th><th>Qty</th><th>&nbsp;</th></tr>' +
                            '</thead>' +
                            '<tbody id="selected-procedures">' +
                            '</tbody>' +
                        '</table>' +
                    '</div>' +
                '</div>' +
            '</div>');
    } catch (e) { LOG(e); }

    $('html, body').animate({
        scrollTop: $(".check-in-action-title").first().offset().top - 100
    }, 500);
});
$('#check-in-datatable').on('click', '.request-pa', function () {
    setCount = 0;
    noAuthType = 'request-pa';
    $('#selected-pa-procedures').html('');
    $('.m-d-back').click();

    // reset content of diagnosis selection for both PA and No-Auth requests
    $('#temp-no-auth-diagnosis-list').html('');
    $('#no-auth-diagnosis-list').html('');
    $('#temp-pa-request-diagnosis-list').html('');
    $('#pa-request-diagnosis-list').html('');

    // reset content of procedure selection for both PA and No-Auth requests
    $('#selected-procedures').html('');
    $('#select-procedures').html('');

    var $e = $(this);
    var enrolleeNumber = $e.parent().next().next().text().split('/');
    selectedMember.name = $e.parent().next().text();
    selectedMember.encounterDate = $e.parent().next().next().next().text();
    
    selectedMember.legacyCode = enrolleeNumber[0];
    selectedMember.dependantId = enrolleeNumber[1];
    
    //$('body').find('.introjs-nextbutton').first().click();
    try {
        $('.check-in-fullname').html(selectedMember.name + " (" + selectedMember.legacyCode + "/" + selectedMember.dependantId + ")");
        $('.check-in-encounter-date').html(selectedMember.encounterDate);
        $('.empty-container').addClass('hide');
        $('.no-auth-submission-container').addClass('hide');
        $('.pa-requester-container').removeClass('hide');

        // Hide the other procedure table
        $('#f-p-c').addClass('hide');

        // Show All Here
        $('#a-p-c').removeClass('hide');

        $('#request-params').html(
            '<div>' +
            '<div style="border: 1px solid #ccc;background: whitesmoke;padding: 15px;max-height: 60px;">' +
            '<h4 class="pull-left">Diagnosis <small> - Add one or more diagnosis here</small></h4>' +
            '<a class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#diagnosis-selection" style="top: -36px;position: relative;">Add&nbsp;<i class="fa fa-plus"></i></a>' +
            '<br style="clear: both;">' +
            '</div>' +
            '<div style="border: 1px solid #ccc;border-top: 0;padding: 15px;">' +
            '<ul id="pa-request-diagnosis-list"></ul>' +
            '</div><br>' +

            '<div style="border: 1px solid #ccc;background: whitesmoke;padding: 15px;max-height: 60px;">' +
            '<h4 class="pull-left">Procedure <small> - Add one or more procedure here</small></h4>' +
            '<a class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#procedure-selection" style="top: -36px;position: relative;">Add&nbsp;<i class="fa fa-plus"></i></a>' +
            '<br style="clear: both;">' +
            '</div>' +
            '<div style="border: 1px solid #ccc;border-top: 0;padding: 15px;">' +
            '<div id="pa-request-procedure-list">' +
            '<table class="table">' +
            '<thead>' +
            '<tr><th>Procedure</th><th>Code</th><th>Amount</th><th>Qty</th><th>&nbsp;</th></tr>' +
            '</thead>' +
            '<tbody id="selected-pa-procedures">' +
            '</tbody>' +
            '</table>' +
            '</div>' +
            '</div>' +
            '</div>');
    } catch (e) { LOG(e); }

    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        paRequestedBefore = true;
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        var procedures = "";
        for (var d = 0; d < data.length; d++) {
            procedures += "<tr><td><a href='javascript:void(0);' class='text-primary' onclick='SelectProcedure(\"" + data[d].procedureCode + "\", \""
                + data[d].procedureDesc.replace(/'/g, '&sq;').replace(/"/g, '&dq;').replace(/\n/g, '') + "\", this)'>Select</a></td><td>" + data[d].procedureCode + "</td><td>" + data[d].procedureDesc +
                "</td></tr>";
        };

        $('#procedure-list1').html(procedures);
        $('.procedure-loading').addClass('hide');

        // Hide All procedures list
        $('#a-p-c').addClass('hide');
        $('#add-set').addClass('hide');

        // Show filtered Here
        $('#f-p-c').removeClass('hide');

        if (paRequestedBefore === true)
            $('#filtered-procedure-content').DataTable().destroy();
        $('#filtered-procedure-content').DataTable({
            "order": [[2, "desc"]]
        });
    };
    var data = {
        legacyCode: selectedMember.legacyCode,
        dependantId: selectedMember.dependantId,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    }
    $('.procedure-loading').removeClass('hide');
    POSTToServer2(procedureListURL, data, success, error);
    $('html, body').animate({
        scrollTop: $(".check-in-action-title").first().offset().top + 520
    }, 300);

});
function LoadPARequests() {
    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {

        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        //if (data.message !== undefined) {
        //    //LoadPARequests();
        //}

        ParsePARequestData(data);
    };
    var data = {
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.pa-request-loading').removeClass('hide');
    POSTToServer2(paRequestURL, data, success, error);
}
function ParsePARequestData(data) {
    var res = '';
    for (var d = 0; d < data.length; d++) {
        var item = data[d];

        var isPicked = (item.isPicked === true) ? "<label class='label label-success'>Picked</label>" : "<label class='label label-danger'>False</label>";
        var isIssued = (!isNaN(item.paNumber) && item.paNumber !== null);

        var status = "";
        if (isIssued === true && !item.isOpen === true) {
            status = "<label class='label label-success'>Approved</label>";
        }
        else if (!isIssued && !item.isOpen) {
            status = "<label class='label label-danger'>Denied</label>";
        }
        else {
            status = "<label class='label label-default'>Pending</label>";
        }


        res += "<tr><td>" + item.requestTracker + "</td>";
        res += "<td>" + item.enrolleeNumber + "/" + item.dependantId + "</td>";
        res += "<td class=''>" + item.requestDate + "</td>";
        res += "<td class='hide'>" + isPicked + "</td>";
        res += "<td class='hide'>" + item.timePicked + "</td>";
        res += "<td>" + status + "</td>";
        res += "<td class='hide'>" + item.paNumber + "</td>";
        res += "<td class='hide'>" + item.resolutionTime + "</td>";
        res += "<td><a href='javascript:void(0);' onclick='ViewPARequestDetails(\"" + item.requestTracker + "\", \"" + item.enrolleeNumber + "\", \"" + item.dependantId + "\")' data-toggle='modal' data-target='#request-details-modal'>Details <i class='fa fa-eye-open'></i></a></td></tr>";
    };

    $('#request-list').html(res);
    $('#request-list').parent().DataTable({
        dom: 'Bfrtip',
        buttons: [
            'excel', 'csvHtml5'
        ],
        "order": [[2, "desc"]]
    });
    $('.pa-request-loading').addClass('hide');

    $('.page-body').removeClass('hide');
    $('.loading-complete').addClass('hide');
    
    var intro = localStorage.getItem("intro");
    if (intro === null) {
        var c = confirm('Would you like a tour of the app?');
        if (!c) {
            localStorage.setItem("intro", "skipped");
            NotifySuccess('Guided Tour', 'Ok. We will skip the tour for now. Remember that you can initiate the tour at any time using the button on the top-right corner');
        }
    }

    LogEvent("Page Load Complete", "Page loading completes - Next Function ShowMemberIntro()");
    ShowMemberIntro();
}


//=======================================================================================================
// TOUR
function ShowMemberIntro() {
    //RemoveExistingIntro();
    var intro = localStorage.getItem("intro");
    
    if (intro !== null && introInitiated === false) {
        LogEvent("Member Intro", "Member Intro has either been skipped or run through");
        return;
    }

    introJs().setOption('showProgress', true).start().oncomplete(function () {
        $('.intro').each(function () {
            $(this).removeAttr('data-intro')
                .removeAttr('data-step')
                .removeAttr('data-hint');
        });
        LogEvent("Member Intro", "Running through member intro");
    });
}
function ShowMemberSearchResultIntro() {
    RemoveExistingIntro();

    // Check if first time
    if (localStorage.getItem("intro") !== null && introInitiated === false) {
        LogEvent("Member Search Intro", "Member Search Intro has either been skipped or run through");
        return;
    }

    // Set up properties
    $('.member-search-error').attr("data-intro", "This displays the number of results returned").attr("data-step", "3").attr("data-hint", "Number of results");
    $('.member-results').attr("data-intro", "Results are displayed here").attr("data-step", "4").attr("data-hint", "Rresult view");
    $('.view-member').first().attr("data-intro", "Click here to see member detail").attr("data-step", "5").attr("data-hint", "View member details");

    introJs().setOption('showProgress', true).start().oncomplete(function () {
        $('.intro').each(function () { $(this).removeAttr('data-intro').removeAttr('data-step').removeAttr('data-hint').removeClass('intro'); });
        LogEvent("Member Search Intro", "Running through member search intro");
    });
}
function ShowMemberDetailsIntro() {
    // Check if first time
    if (localStorage.getItem("intro") !== null && introInitiated === false) return;


    //$('.intro').removeAttr('data-intro').removeAttr('data-step').removeAttr('data-hint').removeClass('intro');

    // Set up properties
    //$('.mci').attr("data-intro", "This displays enrollee eligibility. Please remember to click the check-in button.").attr("data-step", "1").attr("data-hint", "Eligibility");
    //$('.check-in-enrollee').attr("data-intro", "Don't forgot to check in.<br />Click here to check enrollee in. Member dependants are shown here").attr("data-step", "7").attr("data-hint", "Eligibility");
    //introJs().setOption('showProgress', true).start().oncomplete(function () { $('.mci').removeAttr('data-intro').removeAttr('data-step').removeAttr('data-hint').removeClass('intro'); });
}
function ShowCheckInDetailsIntro() {

    RemoveExistingIntro();

    // Check if first time
    if (localStorage.getItem("intro") !== null && introInitiated === false) {
        LogEvent("Check-In Intro", "Check In Details Intro has either been skipped or run through");
        return;
    }

    // Set up properties  
    $('.dependants').addClass('intro');
    $('.recent-pas').addClass('intro');
    $('.nav-item1').addClass('intro');
    $('.nav-item2').addClass('intro');

    $('.dependants').attr("data-intro", "This enrollee's dependants are listed here if any").attr("data-step", "1").attr("data-hint", "View member dependants");
    $('.recent-pas').attr("data-intro", "This lists all recent PAs issued to selected enrollee for your facility").attr("data-step", "2").attr("data-hint", "Recent PAs");
    $('.nav-item1').attr("data-intro", "Summary of enrollee limits and balance").attr("data-step", "3").attr("data-hint", "Benefits");
    $('.nav-item2').attr("data-intro", "Click here to see the list of covered services for this enrollee.<br />You can use the filter function to narrow the dataset.<br /><br />Click '<strong>Show Tariff</strong>' to see associated amount").attr("data-step", "4").attr("data-hint", "Covered Services");
    introJs().setOption('showProgress', true).start().oncomplete(function () {
        $('.intro').each(function () { $(this).removeAttr('data-intro').removeAttr('data-step').removeAttr('data-hint').removeClass('intro'); });
        LogEvent("Check-In Intro", "Running through check-in intro");
    });
}
function ShowNoAuthIntro() {
    
    RemoveExistingIntro();

    // Check if first time
    if (localStorage.getItem("intro") !== null && introInitiated === false) {
        LogEvent("No-Auth Intro", "No-Auth Intro has either been skipped or run through");
        return;
    };

    // Set up properties    
    $('.check-in-form')
        .attr("data-intro", "Use this form to retrieve check-in data. between dates.<br /><strong>Please note that you can only pull check-in data for three days at a time.</strong>")
        .attr("data-step", "1")
        .attr("data-hint", "View member details");

    $('a.request-pa').first()
        .attr("data-intro", "Click here to request PA for this enrollee")
        .attr("data-step", "2")
        .attr("data-hint", "Request PA action button");

    $('.pa-requester-new')
        .attr("data-intro", "This is where you request a new PA for an enrollee.<br /><strong>Please note that you must have clicked on the 'Request PA' button for an enrollee from the list of checked in enrollees on the left pane.</strong>")
        .attr("data-step", "3")
        .attr("data-hint", "Request new PA area");

    $('.clear-diagnosis')
        .attr("data-intro", "Click here to add new diagnosis/procedure set(s)")
        .attr("data-step", "4")
        .attr("data-hint", "Add diagnosis/procedure set");

    introJs().setOption('showProgress', true)
        .setOption('tooltipPosition', 'auto')
        .setOption('positionPrecedence', ['bottom', 'left', 'right', 'top'])
        .start()
        .oncomplete(function () {
            introJs().setOption('showProgress', true).start().oncomplete(function () {
                $('.intro').each(function () { $(this).removeAttr('data-intro').removeAttr('data-step').removeAttr('data-hint').removeClass('intro'); });
                LogEvent("No-Auth Intro", "Running through no-auth intro");
            });
        });
}
function ShowDiagnosisSelectionIntro() {
    RemoveExistingIntro();

    // Check if first time
    if (localStorage.getItem("intro") !== null && introInitiated === false) {
        LogEvent("Diagnosis Intro", "Diagnosis Intro has either been skipped or run through");
        return;
    }

    $('#selected-diagnosis').addClass('intro');
    $('#selected-procedures').addClass('intro');
    $($('#m-d-container').find('input')[0]).addClass('intro');
    $('#diagnosis-list').find('a').first().addClass('intro');

    // Set up properties    
    $('#selected-diagnosis').attr("data-intro", "Selected diagnosis will be added here").attr("data-step", "1").attr("data-hint", "Diagnosis View");
    $('#selected-procedures').first().parent().attr("data-intro", "You can add multiple procedures here").attr("data-step", "2").attr("data-hint", "Add procedures");
    $($('#m-d-container').find('input')[0]).attr("data-intro", "Type here to filter diagnosis").attr("data-step", "3").attr("data-hint", "Filter diagnosis");
    $('#diagnosis-list').find('a').first().attr("data-intro", "Click <strong>'Select'</strong> here to select a diagnosis. <strong class='text-red'>You can only select one diagnosis per procedure set, however you can create multiple sets</strong>").attr("data-step", "4").attr("data-hint", "Select diagnosis");
    introJs().setOption('showProgress', true).start().oncomplete(function () {
        $('.intro').each(function () { $(this).removeAttr('data-intro').removeAttr('data-step').removeAttr('data-hint').removeClass('intro'); });
        LogEvent("Diagnosis Intro", "Running through diagnosis intro");
    });
}
function ShowProcedureSelectionIntro() {
    RemoveExistingIntro();
    // Check if first time
    if (localStorage.getItem("intro") !== null && introInitiated === false) {
        LogEvent("Procedure Intro", "Diagnosis Intro has either been skipped or run through");
        return;
    }

    $('#procedure-list1').find('a').first().attr("data-intro", "Click <strong>'Select'</strong> here to select <strong>'Multiple'</strong> procedures").attr("data-step", "1").attr("data-hint", "Select diagnosis");
    introJs().setOption('showProgress', true).start().oncomplete(function () {
        $('.intro').each(function () { $(this).removeAttr('data-intro').removeAttr('data-step').removeAttr('data-hint').removeClass('intro'); }); 
        LogEvent("Procedure Intro", "Running through diagnosis intro");
    });
}
function RemoveExistingIntro() {
    $('body').find('a[data-intro],div[data-intro],table[data-intro]').each(function () {
        $(this).removeAttr('data-intro').removeAttr('data-step').removeAttr('data-hint');
    });
    $('.intro').removeAttr('data-intro').removeAttr('data-step').removeAttr('data-hint');
}

//=============================================================================================================
function ViewPARequestDetails(requestTracker, legacyCode, dependantId) {
    $('#pa-request-details-cont').html('');
    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {

        try { data = window.JSON.parse(data); } catch (e) { LOG(data); }
        $('#pa-request-details-cont').html(data.result);
        $('.pa-request-details-loading').addClass('hide');
        LogEvent("View PA Request Details", "");
    };
    var data = {
        requestTracker: requestTracker,
        legacyCode: legacyCode,
        dependantId: dependantId,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.pa-request-details-loading').removeClass('hide');
    POSTToServer2(paIssueDetailsURL, data, success, error);
    
}
function SelectFilteredProcedure(procedureCode, procedureDesc, e) {
    var justAdded = $('#selected-procedures').text();
    var existing = $('#request-params').text();

    // Check that it has not been added to this ques or already added sets
    if (justAdded.indexOf(procedureCode) !== -1 || existing.indexOf(procedureCode) !== -1) {
        NotifyDanger('Notification', 'That procedure has been added');
        return;
    }

    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(data); }
        var limit = data.limit;

        var tariff = parseInt(data.tariff.tariff);

        var options = "";
        for (var ix = 0; ix < limit; ix++) {
            if (ix === 0)
                options += "<option selected='selected'>" + (ix + 1) + "</option>";
            else
                options += "<option>" + (ix + 1) + "</option>";
        }

        var readonly = (!isNaN(tariff) ? "" : "readonly='readonly' ");
        var value = "value='" + (!isNaN(tariff) ? '' : tariff) + "'";
        //if (procedureCode == "90275" || procedureCode == "V2020") {
        //    value = "value='' ";
        //    readonly = "";
        //}
        var res =
            "<tr>" +
            "<td style='width: 60%;'>" + jsUcfirst(procedureDesc.replace(/&sq;/g, '\'').replace(/&dq;/g, '"')) + "</td>" +
            "<td>" + procedureCode + "</td>" +
            "<td><input style='width: 78px;padding: 5px;height: 24px;' class='form-control' " + value + readonly + " /></td>" +
            "<td><select class='form-control' style='width: 60px;padding: 5px;height: 28px;margin-top: -4px;'>" + options + "</select></td>" +
            "<td><a class='text-c-red' onclick='RemoveProcedureSet(this);' href='javascript:void(0);'><i class='fa fa-trash'></i></a></td>" +
            "</tr>";
        if ($('#selected-procedures').text().trim().indexOf("No procedures selected") !== -1) {
            $('#selected-procedures').html('');
        }
        $('#add-set').removeClass('hide');
        $('#selected-procedures').append(res);
        $('.diagnosis-loading').addClass('hide');

        LogEvent("Select Filtered Procedure", "");
    };

    var diagnosisCode = ($('#selected-diagnosis').text().split('//'))[1];
    if (diagnosisCode === undefined || diagnosisCode === '') {
        NotifyDanger('Notification', 'Please select a diagnosis');
        return false;
    }
    var data = {
        providerId: __ProviderId,
        legacyCode: selectedMember.legacyCode,
        dependantId: selectedMember.dependantId,
        procedureCode: procedureCode,
        isNoAuthRequest: true,
        isPARequest: (noAuthType !== 'submit-no-auth'),
        diagnosisCode: diagnosisCode,
        __RequestVerificationToken: token
    };
    $('.diagnosis-loading').removeClass('hide');
    POSTToServer2(PARulesTestWithTariffURL, data, success, error);
}
$('#request-pa').click(function () {
    var allDiagnosis = [];
    var allProcedures = [];
    var hasErrors = false;

    $('.selected-disgnosis').each(function () {
        allDiagnosis.push($(this).text());
    });
    $('.selected-procedurecode').each(function () {
        var $procedure = $(this);
        var $amount = $procedure.next();
        var $qty = $amount.next();

        var amount = parseFloat($amount.children().first().val());
        hasErrors = amount === 0 || isNaN(amount);
        allProcedures.push(
            $procedure.text() +
            "#ps#" + amount +
            "#ps#" + parseInt($qty.children().first().val())
        );
    });
    if (hasErrors === true) {
        NotifyDanger('Notification', 'Please provide an amount for each item');
        return;
    }
    if (allDiagnosis.length === 0) {
        NotifyDanger("Notification", "Please at least one diagnosis");
        return;
    }
    if (allProcedures.length === 0) {
        NotifyDanger("Notification", "Please at least one procedure");
        return;
    }

    var allItems = allDiagnosis.join() + "#cs#" + allProcedures.join();

    

    var d = (($('.check-in-fullname').text().split('('))[1].split(')'))[0];
    var enrollee = d.split('/');

    var procedureDate = $('#procedure-date').val();
    if (procedureDate === '') {
        NotifyDanger("Notification", "Please provide a procedure date");
        return;
    }

    $('#request-pa').addClass('hide');
    $('#check-in-action-loading').removeClass('hide');

    $('#check-in-action-loading').removeClass('hide');
    var data = {
        legacyCode: enrollee[0],
        dependantId: enrollee[1],
        providerId: __ProviderId,
        procedureDate: procedureDate,
        diagnosisSet: allItems,
        comments: $('#comments').val().trim(),
        __RequestVerificationToken: token
    };

    var error = function (data) {

        $('#request-pa').addClass('hide');
        LOG(data);
    };
    var success = function (data)  {

        $('#check-in-action-loading').addClass('hide');
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        if (data.result === true) {
            NotifySuccess('Notification', 'Your request <strong>' + data.requestTracker + '</strong> was made successfully.');
            $('#request-list').parent().DataTable().destroy();
            $('#request-list').html('');
            ParsePARequestData(data.requests);
        }
        else {
            NotifyDanger('Notification', 'Your request failed for some reason. Please raise a ticket to resolve this issue.');
        }
        
        $('.remove-diagnosis-set').each(function () {
            $(this).click();
        });
        $('#comments').val('');
        $('#check-in-action-loading').addClass('hide');
        $('#request-pa').removeClass('hide');
        LogEvent("Requesting PA Issuance", "");
    };
    POSTToServer2(requestPAURL, data, success, error);

});
function ScrollRequesterIntoView() {

    $('html,body').animate({ scrollTop: $('.pa-requester-container').offset().top + 460 }, 300);
}

var $el;
$('#pa-procedure-exc-content').on('click', '.exec-pa', function () {
    var submitted = $($(this).children()[12]).text();

    if (submitted === 'True') {
        NotifyDanger('Notification', 'This PA has been submitted and cannot be processed further');
        return false;
    }
    $el = $(this);

    $('#claim-proc-desc, #claim-proc-exc-desc').text($($(this).children()[7]).text());
    $('#claim-proc-code, #claim-proc-exc-code').val($($(this).children()[8]).text());
    LogEvent("Exclusion process initialization", "");
});
$('#pa-procedure-content').on('click', '.exec-pa', function () {
    var submitted = $($(this).children()[13]).text();

    if (submitted === 'True') {
        NotifyDanger('Notification', 'This PA has been submitted and cannot be processed further');
        return false;
    }
    $el = $(this);

    $('#claim-proc-desc, #claim-proc-exc-desc').text($($(this).children()[8]).text());
    $('#claim-proc-code, #claim-proc-exc-code').val($($(this).children()[9]).text());
    LogEvent("PA process initialization", "");
});
function ResetPASubmitContent(resetPa) {
    if (resetPa === true) {
        var result = '<div class="col-sm-4">' +
                '<img src="https://online.hygeiahmo.com/hyprovider/Content/Images/default-avatar-sm.png" style="width: 100%;" class="img-radius align-top m-r-15 member-image" />' +
            '</div>' +
            '<div class="col-sm-8">' +
                '<div class="m-t-10">' +
                    '<table style="width: 100%;">' +
                        '<tbody>' +
                            '<tr><th class="member-fullname" colspan="2"><h5>----- ----- -----</h5></th></tr>' +
                            '<tr><th>Enrollee Num.</th><th class="member-number" colspan="1"><span class="text-primary">-----/-</span></th></tr>' +
                            '<tr><th>Plan</th><td class="member-plan">----- ----- ----</td></tr>' +
                            '<tr><th>Hospital</th><td class="member-hospital">----- ----- -----</td></tr>' +
                            '<tr><th>Is Capitated?</th><td class="member-capitation">-------</td></tr>' +
                        '</tbody>' +
                    '</table>' +
                '</div>' +
            '</div>';
        $('#pa-member-details').html(result);
        $('#exc-member-details').html(result);

        $('#pa-procedure-content').html('<tr class="highlight" style="white-space:nowrap;"><td colspan="16">Search for PA to continue</td></tr>');
        $('#pa-procedure-exc-content').html('<tr class="highlight" style="white-space:nowrap;"><td colspan="16">Search for PA to continue</td></tr>');
        $('#claim-proc-code').val('');
        $('#claim-proc-exc-code').val('');
        $('#enc-date-from').val('');
        $('#enc-date-exc-from').val('');
        $('#enc-date-to').val('');
        $('#enc-date-exc-to').val('');
    }
    $('#billed-amount').val('');
    $('#billed-exc-amount').val('');
    $('#claim-proc-desc').text('');
    $('#claim-proc-exc-desc').text('');
}
function SetPAEncounterMaxDate(e){
    var t = $(e).val();
    document.getElementById("enc-date-to").setAttribute("min", t);
}

var PreAuthDataRetrived = false;
var ExclusionsDataRetrived = false;
var currentCLClaim;
function GetPreAuthData() {

    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }


        if (data.summary.length > 0) {
            var preAuthList = data.claims;
            var preAuthSummary = data.summary[0];

            $('#pre-auth-total-sum').text(FormatMoney(preAuthSummary.totalCharge));
            $('#pre-auth-approved-sum').text(FormatMoney(preAuthSummary.approved));
            $('#pre-auth-denied-sum').text(FormatMoney(preAuthSummary.denied));
            $('#pre-auth-discounted-sum').text(FormatMoney(preAuthSummary.discounted));

            var claimLines = "";
            for (var i = 0; i < preAuthList.length; i++) {

                claimLines += "<tr>" +
                    "<td>" + jsUcfirst(preAuthList[i].fullName) + "</td>" +
                    "<td>" + FormatMoney(preAuthList[i].totalCharge) + "</td>" +
                    "<td>" + FormatMoney(preAuthList[i].approved) + "</td>" +
                    "<td>" + FormatMoney(preAuthList[i].denied) + "</td>" +
                    "<td>" + ((preAuthList[i].initialStatus === 1 && preAuthList[i].finalStatus !== 2) ? '<label class="label label-success">True</label>' : '<label class="label label-danger">False</label>') + "</td>" +
                    "<td class='hide'>" + FormatDate(preAuthList[i].encouterDateFrom) + "</td>" +
                    "<td class='hide'>" + FormatDate(preAuthList[i].encouterDateTo) + "</td>" +
                    "<td class='hide'>" + preAuthList[i].procedureCode + "</td>" +
                    "<td class='hide'>" + jsUcfirst(preAuthList[i].procedureDesc) + "</td>" +
                    "<td class='hide'>" + preAuthList[i].legaCycode + "</td>" +
                    "<td class='hide'>" + preAuthList[i].dependantId + "</td>" +
                    '<td>' +
                    '<a class="btn btn-outline-danger waves-effect waves-light btn-sm btn-smaller" style="font-size: 12px;" onclick="RemoveCLClaim(this, \'' + preAuthList[i].claimNumber + '\', \'' + preAuthList[i].endorsementkey + '\')">Remove</a>&nbsp;' +
                    '<a class="btn btn-outline-secondary waves-effect waves-light btn-sm btn-smaller" style="font-size: 12px;" onclick="ViewCLClaimDetails(this, \'' + preAuthList[i].claimItemId + '\', \'' + preAuthList[i].paNumber + '\')" data-toggle="modal" data-target="#pc-claim-modal">More</a></td>' +
                    "<td class='hide'>" + preAuthList[i].diagnosisCode + "</td>" +
                    "<td class='hide'>" + jsUcfirst(preAuthList[i].diagnosisDesc) + "</td>" +
                    "<td class='hide'>" + preAuthList[i].claimItemId + "</td>" +
                    "<td class='hide'>" + FormatDate(preAuthList[i].dateAdded) + "</td>" +
                    //"<td class='hide'>" + preAuthList[i].endorsementkey + "</td>" +
                    //"<td class='hide'>" + preAuthList[i].panumber + "</td>" +
                    //"<td class='hide'>" + preAuthList[i].endorsementvalue + "</td>" +
                    //"<td class='hide'>" + preAuthList[i].balance + "</td>" +
                    //"<td class='hide'>" + preAuthList[i].memberid + "</td>" +
                    "</tr>";
            }

            if (PreAuthDataRetrived === true) {
                $('#pre-auth-list').DataTable().destroy();
            }

            $('#pre-auth-list tbody').html(claimLines);
            $('#pre-auth-list').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5', 'csvHtml5'
                ],
                "order": [[14, "desc"]]
            });
            PreAuthDataRetrived = true;

            LogEvent("Getting Pre-Auth Data", preAuthList.length + " records retrieved");
        }
        else {

            if (PreAuthDataRetrived === true) {
                $('#pre-auth-list').DataTable().destroy();
            }
            PreAuthDataRetrived = false;
            $('#pre-auth-total-sum').text(FormatMoney(0));
            $('#pre-auth-approved-sum').text(FormatMoney(0));
            $('#pre-auth-denied-sum').text(FormatMoney(0));
            $('#pre-auth-discounted-sum').text(FormatMoney(0));
            $('#pre-auth-list tbody').html("<tr><td colspan='15'>No ubsubmitted claims available</td></tr>");

            LogEvent("Getting Pre-Auth Data", "No records retrieved");
        }

        $('.pre-auth-list-loading').addClass('hide');
    };
    var data = {
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.pre-auth-list-loading').removeClass('hide');
    POSTToServer2(preAuthListURL, data, success, error);
}
function ViewCLClaimDetails(e, claimItemId, paNumber) {
    currentCLClaim = e;

    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        var $depId = $(currentCLClaim).parent().prev();
        var $encDateTo = $depId.prev().prev().prev().prev();

        var $legCode = $depId.prev();
        var $procDesc = $legCode.prev();
        var $procCode = $procDesc.prev();
        var $encDate = $procCode.prev();
        var $name = $encDate.prev().prev().prev().prev().prev();
        var $diagCode = $(currentCLClaim).parent().next();
        var $diagDesc = $diagCode.next();

        $('.claim-enrollee-name').text($name.prev().text());
        $('.claim-enrollee-number').text($legCode.text() + "/" + $depId.text());
        $('.claim-encounter-date').html($encDate.text() + " <strong>To</strong> " + $encDateTo.text());
        $('.claim-diagnosis').text($diagDesc.text() + " // " + $diagCode.text());
        $('.claim-procedures').text($procDesc.text() + " // " + $procCode.text());
        $('#claim-panumber').text(" - (PA Number: " + paNumber + ")");

        var diagnosis = data.diagnosis;

        var diag = "";
        for (var i = 0; i < diagnosis.length; i++) {
            if ($diagDesc.text().indexOf(diagnosis[i].diagnosisCode) === -1)
                diag += jsUcfirst(diagnosis[i].diagnosisDesc) + " // " + diagnosis[i].diagnosisCode + "<br />";
        }

        var proc = "";
        var procedures = data.procedures;
        for (var j = 0; j < procedures.length; j++) {
            if ($procDesc.text().indexOf(procedures[j].procedureCode) === -1)
                proc += jsUcfirst(procedures[j].procedureDesc) + " // " + procedures[j].procedureCode + "<br />";
        }
        $('.claim-diagnosis').html(diag);
        $('.claim-procedures').html(proc);

        $('#denial-reasons').html(data.denialReasons);
        LogEvent("View Claim Details", "");

        currentCLClaim = undefined;
        $('.pre-auth-list-loading').addClass('hide');
    };
    var data = {
        claimItemId: claimItemId,
        __RequestVerificationToken: token
    };
    $('.pre-auth-list-loading').removeClass('hide');
    POSTToServer2(preAuthViewClaimURL, data, success, error);
}
function RemoveCLClaim(e, claimnumber, endorsementkey) {

    if (!confirm('Are you sure?'))
        return false;

    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        if (data.success === true) {
            NotifySuccess('Notification', 'Claim was removed successfully.');

            if (PreAuthDataRetrived === true) {
                $('#pre-auth-list').DataTable().destroy();
            }
            $('#pre-auth-list tbody').html('');
            GetPreAuthData(false);
        }
        else {
            NotifyDanger('Notification', 'Unable to remove claim.');
            LOG(data.error);
        }
        $('.pre-auth-list-loading').addClass('hide');
        LogEvent("Removed Claim", "Removed claim with claim number " + claimnumber);   
    };
    var data = {
        claimnumber: claimnumber,
        endorsementkey: endorsementkey,
        __RequestVerificationToken: token
    };
    $('.pre-auth-list-loading').removeClass('hide');
    POSTToServer2(preAuthRemovalURL, data, success, error);
}
$('#btn-find-pa').click(function () {
    var pa = $('#find-pa').val().trim();
    if (pa === '' || isNaN(pa)) {
        NotifyDanger('Notification', 'Please provide a PA Number');
        return false;
    }
    ResetPASubmitContent();
    $('.find-pa-loading').removeClass('hide');
    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {

        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        if (data.message !== undefined) {
            NotifyDanger('Notification', data.message);
            return false;
        }

        SetMemberBasics(data.eligiblity);
        $('.member-fullname1').html("<h5>" + jsUcfirst(data.eligiblity.eligibilityInfo.lastName) + ' ' + jsUcfirst(data.eligiblity.eligibilityInfo.firstName) + ' ' + jsUcfirst(data.eligiblity.eligibilityInfo.othername) + "</h5>");
        $('.member-number1').html("<span class='text-primary'>" + data.eligiblity.eligibilityInfo.legacyCode + '/' + data.eligiblity.eligibilityInfo.dependantId + "</span>");
        $('.member-plan1').html(jsUcfirst(data.eligiblity.eligibilityInfo.plan));
        $('.member-hospital1').html(jsUcfirst(data.eligiblity.eligibilityInfo.provider));
        if (data.eligiblity.isCapitated === true) {
            $('.member-capitation1').html('<label class="label label-primary">True</label>');
        } else {
            $('.member-capitation1').html('<label class="label label-default">False</label>');
        }

        var procedures = data.paProcedures;
        var result = '';
        for (var i = 0; i < procedures.length; i++) {
            result += '<tr class="exec-pa" style="cursor: pointer;">' +
                '<td>Select</td>' +
                '<td class="hide">' + procedures[i].endorsementkey + '</td>' +
                '<td class="hide">' + procedures[i].endorsementvalue + '</td>' +
                '<td class="hide">' + procedures[i].panumber + '</td>' +
                '<td class="hide">' + procedures[i].memberid + '</td>' +
                '<td class="hide">' + procedures[i].legacycode + '</td>' +
                '<td class="hide">' + procedures[i].dependantid + '</td>' +
                '<td class="hide">' + procedures[i].balance + '</td>' +
                '<td title="' + jsUcfirst(procedures[i].proceduredesc) + '">' + jsUcfirst(procedures[i].proceduredesc) + '</td>' +
                '<td class="hide">' + procedures[i].procedurecode + '</td>' +
                '<td class="hide">' + procedures[i].amountgranted + '</td>' +
                '<td class="hide">' + procedures[i].receiveddate + '</td>' +
                '<td class="rgHeader">' + ((procedures[i].newclaimnumber !== null) ? "<label class='label label-info'>True</label>" : "<label class='label label-danger'>False</label>") + '</td>' +
                '<td class="rgHeader">' + ((procedures[i].submitted === true) ? "<label class='label label-info'>True</label>" : "<label class='label label-danger'>False</label>") + '</td>' +
                '<td class="hide">' + procedures[i].submittedby + '</td>' +
                '<td class="hide">' + procedures[i].diagnosiscode + '</td>' +
                '<td class="hide">' + procedures[i].newclaimnumber + '</td>' +
                '</tr>';
        }
        $('#pa-procedure-content').html(result);

        // Show member image id available
        $('.member-image1').prop('src', data.eligiblity.eligibilityInfo.picture);

        $('.find-pa-loading').addClass('hide');
        LogEvent("Search PA", "Searching for PA: " + pa);  
    };
    var data = {
        providerId: __ProviderId,
        paNumber: pa,
        isExclusion: false,
        __RequestVerificationToken: token
    };
    $('.pa-request-loading').removeClass('hide');
    POSTToServer2(paFetchDetailsURL, data, success, error);
});
$('#btn-save-pa').click(function () {

    var edf = $('#enc-date-from').val();
    var edt = $('#enc-date-to').val();
    if (edf.trim() === '' || edt.trim() === '')
    {
        NotifyDanger('Notification', 'Please provide encounter dates');
        return false;
    }

    var charge = $('#billed-amount').val();
    if (charge.trim() === '') {
        NotifyDanger('Notification', 'Please provide a charge amount');
        return false;
    }
    var procedurecode = $($el.children()[9]).text();
    if (procedurecode.trim() === '') {
        NotifyDanger('Notification', 'Please provide select a procedure');
        return false;
    }

    var endorsementkey = $($el.children()[1]).text();
    var pa = $($el.children()[3]).text();
    var procedureamount = $($el.children()[2]).text();
    var bal = $($el.children()[7]).text();
    var memberid = $($el.children()[4]).text();
    var legacycode = $($el.children()[5]).text();
    var dependantid = $($el.children()[6]).text();
    var diagnosis = $($el.children()[15]).text();
    var submitted = $($el.children()[13]).text();
    var submittedby = $($el.children()[14]).text();

    if (submitted === "True") {
        NotifyDanger('Notification', 'This claim has already been submitted and can no longer be reprocessed.');
        return;
    }
    if (submittedby === "inhouseadjusters") {
        NotifyDanger('Notification', 'Stop! This claim has already been processed in house.');
        return;
    }



    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        
        if (data.message !== undefined)
            NotifyDanger('Notification', data.message);
        else
        {
            NotifySuccess('Notification', data);
            if (PreAuthDataRetrived === true) {
                $('#pre-auth-list').DataTable().destroy();
            }
                $('#pre-auth-list tbody').html('');
            GetPreAuthData(false);
            ResetPASubmitContent(false);
        }
    }
    var data = {
        endorsementkey: endorsementkey,
        panumber: pa,
        procedureamount: procedureamount,
        procedurecode: procedurecode,
        balance: ((bal === 'null' || bal === null) ? 0 : bal),
        memberid: memberid,
        legacycode: legacycode,
        dependantid: dependantid,
        diagnosis: diagnosis,
        submitted: submitted,
        submittedby: ((submittedby === 'null' || submittedby === null) ? '' : submittedby),
        chargeamount: charge,
        providerid: __ProviderId,
        encounterdatefrom: edf,
        encounterdateto: edt,
        __RequestVerificationToken: token
    };
    $('.pa-request-loading').removeClass('hide');
    POSTToServer2(submitPAURL, data, success, error);
});
function GetExclusionData() {

    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        var excRes = ParseExclusionData(data);

        if (ExclusionsDataRetrived === true) {
            $('#pre-auth-exc-list').DataTable().destroy();
        }
        $('#pre-auth-exc-list tbody').html(excRes);
        $('#pre-auth-exc-list').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5', 'csvHtml5'
            ]
        });
        ResetPASubmitContent();

        if (data.length > 0) {
            updateTotals(data);
            $('.pre-auth-list-loading').addClass('hide');
            ExclusionsDataRetrived = true;
        }
        else {
            ExclusionsDataRetrived = false;
            $('#pre-auth-total-exc-sum').text(FormatMoney(0));
            $('#pre-auth-approved-exc-sum').text(FormatMoney(0));
            $('#pre-auth-denied-exc-sum').text(FormatMoney(0));
            $('#pre-auth-discounted-exc-sum').text(FormatMoney(0));
            $('#pre-auth-list tbody').html("<tr><td colspan='15'>No ubsubmitted claims available</td></tr>");
        }
        $('.pre-auth-list-loading').addClass('hide');
        LogEvent("Get Exclusion Data", "Fetching exclusion data");  
    };
    var data = {
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.pre-auth-list-loading').removeClass('hide');
    POSTToServer2(exclusionListURL, data, success, error);
}
function ViewExclusionDetails(e, claimItemId, paNumber) {
    currentCLClaim = e;

    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        var $depId = $(currentCLClaim).parent().prev();
        var $encDateTo = $depId.prev().prev().prev().prev();

        var $legCode = $depId.prev();
        var $procDesc = $legCode.prev();
        var $procCode = $procDesc.prev();
        var $encDate = $procCode.prev();
        var $name = $encDate.prev().prev().prev().prev().prev();
        var $diagCode = $(currentCLClaim).parent().next();
        var $diagDesc = $diagCode.next();

        $('.claim-enrollee-name').text($name.prev().text());
        $('.claim-enrollee-number').text($legCode.text() + "/" + $depId.text());
        $('.claim-encounter-date').html($encDate.text() + " <strong>To</strong> " + $encDateTo.text());
        $('.claim-diagnosis').text($diagDesc.text() + " // " + $diagCode.text());
        $('.claim-procedures').text($procDesc.text() + " // " + $procCode.text());
        $('#claim-panumber').text(" - (PA Number: " + paNumber + ")");

        var diagnosis = data.diagnosis;

        var diag = "";
        for (var i = 0; i < diagnosis.length; i++) {
            if ($diagDesc.text().indexOf(diagnosis[i].diagnosisCode) === -1)
                diag += jsUcfirst(diagnosis[i].diagnosisDesc) + " // " + diagnosis[i].diagnosisCode + "<br />"
        }

        var proc = "";
        var procedures = data.procedures;
        for (var j = 0; j < procedures.length; j++) {
            if ($procDesc.text().indexOf(procedures[j].procedureCode) === -1)
                proc += jsUcfirst(procedures[j].procedureDesc) + " // " + procedures[j].procedureCode + "<br />";
        }
        $('.claim-diagnosis').html(diag);
        $('.claim-procedures').html(proc);

        $('#denial-reasons').html(data.denialReasons);
        LogEvent("View Exclusion Detail", "Viewing exlusion with claim ID: " + claimItemId);  

        currentCLClaim = undefined;
        $('.pre-auth-list-loading').addClass('hide');
    };
    var data = {
        claimItemId: claimItemId,
        __RequestVerificationToken: token
    };
    $('.pre-auth-list-loading').removeClass('hide');
    POSTToServer2(preAuthViewClaimURL, data, success, error);
}
function RemoveExclusionClaim(e, claimnumber, endorsementkey) {

    if (!confirm('Are you sure?'))
        return false;

    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        if (data.success === true) {
            NotifySuccess('Notification', 'Claim was removed successfully.');

            if (ExclusionsDataRetrived === true) {
                $('#pre-auth-list').DataTable().destroy();
            }
            $('#pre-auth-list tbody').html('');
            GetExclusionData(false);
        }
        else {
            NotifyDanger('Notification', 'Unable to remove claim.');
            LOG(data.error);
        }
        $('.pre-auth-list-loading').addClass('hide');
        LogEvent("Remove Exclusion", "Removing Exclusion with claim number: " + claimnumber);  
    };
    var data = {
        claimnumber: claimnumber,
        endorsementkey: endorsementkey,
        __RequestVerificationToken: token
    };
    $('.pre-auth-list-loading').removeClass('hide');
    POSTToServer2(exclusionRemovalURL, data, success, error);
}
$('#btn-find-exc-pa').click(function () {
    var pa = $('#find-exc-pa').val().trim();

    if (pa === '' || isNaN(pa)) {
        NotifyDanger('Notification', 'Please provide a PA Number');
        return false;
    }
    ResetPASubmitContent();
    $('.find-pa-loading').removeClass('hide');
    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {

        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        if (data.message !== undefined) {
            NotifyDanger('Notification', data.message);
            $('.find-pa-loading').addClass('hide');
            return false;
        }

        SetMemberBasics(data.eligiblity);
        $('.member-fullname1').html("<h5>" + jsUcfirst(data.eligiblity.eligibilityInfo.lastName) + ' ' + jsUcfirst(data.eligiblity.eligibilityInfo.firstName) + ' ' + jsUcfirst(data.eligiblity.eligibilityInfo.othername) + "</h5>");
        $('.member-number1').html("<span class='text-primary'>" + data.eligiblity.eligibilityInfo.legacyCode + '/' + data.eligiblity.eligibilityInfo.dependantId + "</span>");
        $('.member-plan1').html(jsUcfirst(data.eligiblity.eligibilityInfo.plan));
        $('.member-hospital1').html(jsUcfirst(data.eligiblity.eligibilityInfo.provider));
        if (data.eligiblity.isCapitated === true) {
            $('.member-capitation1').html('<label class="label label-primary">True</label>');
        } else {
            $('.member-capitation1').html('<label class="label label-default">False</label>');
        }

        var procedures = data.paProcedures;
        var result = '';
        for (var i = 0; i < procedures.length; i++) {
            result += '<tr class="exec-pa" style="cursor: pointer;">' +
                '<td class="hide">' + procedures[i].endorsementkey + '</td>' +
                '<td class="hide">' + procedures[i].endorsementvalue + '</td>' +
                '<td class="hide">' + procedures[i].panumber + '</td>' +
                '<td class="hide">' + procedures[i].memberid + '</td>' +
                '<td class="hide">' + procedures[i].legacycode + '</td>' +
                '<td class="hide">' + procedures[i].dependantid + '</td>' +
                '<td class="hide">' + procedures[i].balance + '</td>' +
                '<td title="' + jsUcfirst(procedures[i].proceduredesc) + '">' + jsUcfirst(procedures[i].proceduredesc) + '</td>' +
                '<td class="hide">' + procedures[i].procedurecode + '</td>' +
                '<td class="hide">' + procedures[i].amountgranted + '</td>' +
                '<td class="hide">' + procedures[i].receiveddate + '</td>' +
                '<td class="rgHeader">' + ((procedures[i].newclaimnumber !== null) ? "<label class='label label-info'>True</label>" : "<label class='label label-danger'>False</label>") + '</td>' +
                '<td class="rgHeader">' + ((procedures[i].submitted === true) ? "<label class='label label-info'>True</label>" : "<label class='label label-danger'>False</label>") + '</td>' +
                '<td class="hide">' + procedures[i].submittedby + '</td>' +
                '<td class="hide">' + procedures[i].diagnosiscode + '</td>' +
                '<td class="hide">' + procedures[i].newclaimnumber + '</td>' +
                '</tr>';
        }
        $('#pa-procedure-exc-content').html(result);

        // Show member image id available
        $('.member-image1').prop('src', data.eligiblity.eligibilityInfo.picture);

        $('.find-pa-loading').addClass('hide');
        LogEvent("Search Exclusion PA", "Searching for exclusion with PA: " + pa);
    };
    var data = {
        providerId: __ProviderId,
        paNumber: pa,
        isExclusion: true,
        __RequestVerificationToken: token
    };
    $('.pa-request-loading').removeClass('hide');
    POSTToServer2(paFetchDetailsURL, data, success, error);
});
$('#btn-save-exc-pa').click(function () {

    var edf = $('#enc-date-exc-from').val();
    var edt = $('#enc-date-exc-to').val();
    if (edf.trim() === '' || edt.trim() === '') {
        NotifyDanger('Notification', 'Please provide encounter dates');
        return false
    }

    var charge = $('#billed-exc-amount').val();
    if (charge.trim() === '') {
        NotifyDanger('Notification', 'Please provide a chargge amount');
        return false;
    }

    var endorsementkey = $($el.children()[0]).text();
    var pa = $($el.children()[2]).text();
    var procedureamount = $($el.children()[1]).text();
    var procedurecode = $($el.children()[8]).text();
    var bal = $($el.children()[6]).text();
    var memberid = $($el.children()[3]).text();
    var legacycode = $($el.children()[4]).text();
    var dependantid = $($el.children()[5]).text();
    var diagnosis = $($el.children()[14]).text();
    var submitted = $($el.children()[12]).text();
    var submittedby = $($el.children()[13]).text();

    if (submitted === "True") {
        NotifyDanger('Notification', 'This claim has already been submitted and can no longer be reprocessed.');
        return;
    }
    if (submittedby === "inhouseadjusters") {
        NotifyDanger('Notification', 'Stop! This claim has already been processed in house.');
        return;
    }



    var error = function (data) {
        LOG(data);
    }
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        if (data.success !== undefined || data.message !== undefined)
            NotifyDanger('Notification', data.message);
        else {
            NotifySuccess('Notification', 'Exclusion claim processed successfully');

            var excRes = ParseExclusionData(data);

            if (ExclusionsDataRetrived === true) {
                $('#pre-auth-exc-list').DataTable().destroy();
            }
            ExclusionsDataRetrived = true;
            $('#pre-auth-exc-list tbody').html(excRes);
            $('#pre-auth-exc-list').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5', 'csvHtml5'
                ]
            });
            ResetPASubmitContent(false);

            if (data.length > 0) {
                updateTotals(data);
            }
        }
        LogEvent("Save Exclusion Claim", "Saving exclusion PA: " + pa + "; procedure: " + procedurecode);
    };
    var data = {
        panumber: pa,
        procedurecode: procedurecode,
        memberid: memberid,
        encounterdatefrom: edf,
        encounterdateto: edt,
        chargeamount: charge,
        isSingle: true,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.pa-request-loading').removeClass('hide');
    POSTToServer2(submitExclusionPAURL, data, success, error);
});
function ParseExclusionData(data) {
    var res = "";
    for (var i = 0; i < data.length; i++) {
        res += '<tr id="' + data[i].paClaimProceduresId + '">' +
                    '<td class="hide paBatchItemId">' + data[i].paBatchItemId + '</td>' +
                    '<td class="hide">' + data[i].paClaimProceduresId + '</td>' +
                    '<td class="hide">' + data[i].paClaimItemId + '</td>' +
                    '<td class="">' + data[i].lastName + '</td>' +
                    '<td class="">' + data[i].firstName + '</td>' +
                    '<td class="">' + data[i].paNumber + '</td>' +
                    '<td class="text-left date">' + (data[i].encounterStarts.split('T'))[0] + '</td>' +
                    '<td class="text-left date">' + (data[i].encounterEnds.split('T'))[0] + '</td>' +
                    '<td class="hide">' + data[i].batchNumber + '</td>' +
                    '<td class="date">' + (data[i].batchCreationDate.split('T'))[0] + '</td>' +
                    '<td class="hide">' + data[i].adjuster + '</td>' +
                    '<td class="hide">' + data[i].isSubmitted + '</td>' +
                    '<td class="text-left">' + data[i].procedureCode + '</td>' +
                    '<td class="">' + data[i].chargeAmount + '</td>' +
                    '<td class="">' + data[i].deniedAmount + '</td>' +
                    '<td class="">' + data[i].approvedAmount + '</td>' +
                    '<td class="">' + data[i].amountToPay + '</td>' +
                    '<td class="hide">' + data[i].discountToApply + '</td>' +
                    '<td class="">' + ((data[i].isApproved === true) ? '<label class="label label-success">True</label>' : '<label class="label label-danger">False</label>') + '</td>' +
                    '<td class="">' + (data[i].dateAdded.split('T'))[0] + '</td>' +
                    '<td class="text-center hide">' +
                    '    <a href="javascript:void(0)" onclick="DenialReasons(' + data[i].paClaimProceduresId + ')">Denial Reasons</a>' +
                    '</td>' +
                    '</tr>';
    }
    return res;
}
function updateTotals(data) {
    try {
        var charge = 0;
        var approved = 0;
        var denied = 0;
        var discount = 0;
        var count = 0;
        $.each(data, function () {
            charge += this.chargeAmount;
            approved += this.approvedAmount;
            denied += this.deniedAmount;
        });
        discount = ((data[0].discountToApply / 100) * approved);

        $('#pre-auth-total-exc-sum').text('₦ ' + formatNumber(charge));
        //$('#payment-advice-charge').text('₦ ' + formatNumber(charge));

        $('#pre-auth-approved-exc-sum').text('₦ ' + formatNumber(approved));
        //$('#payment-advice-approved').text('₦ ' + formatNumber(approved));
        //$('#PTTL').text('₦ ' + formatNumber(approved));

        $('#pre-auth-denied-exc-sum').text('₦ ' + formatNumber(denied));
        //$('#payment-advice-denied').text('₦ ' + formatNumber(denied));

        $('#pre-auth-discounted-exc-sum').text(formatNumber(discount));
        //$('#payment-advice-discount').text(formatNumber(discount));

        //$('#count').text(data.length);
        //$('#count1').text(data.length);
        //setTimeout(function () {
        //    $('#batchSummary').html('<tr><td>' + $($('.batchNumber')[0]).text() + '</td><td>₦ '
        //        + formatNumber(charge) + '</td><td>₦ '
        //        + formatNumber(approved) + '</td><td>₦ '
        //        + formatNumber(denied) + '</td><td>'
        //        + formatNumber(discount) + '</td><td>Not Submitted</td></tr>');
        //}, 1000);
    } catch (e) {
        //console.log(e);
    }
}

var pc_ClaimsDatatlized = false;
function PullReportSummary(e) {
    var reportType = $('#no-auth-report-type').val();

    if (pc_ClaimsDatatlized === true) {
        $('#pc_claims').DataTable().destroy();
        $('#pc_claims_content').html('');
    }

    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        var rType = "";
        if (reportType === "2") {
            $('#reportTypee').text("No-Auth Batch Items for selected period");
            rType = "No-Auth";
        }
        else if (reportType === "1") {
            $('#reportTypee').text("Pre-Auth Batch Items for selected period");
            rType = "Pre-Auth";
        }
        else {
            $('#reportTypee').text("Exclusion Items for selected period");
            rType = "Exclusion";
        }

        var summary = "<table class='table table-hover'><tr><td>&nbsp;</td><td>Batch Number</td><td>Total Charge</td><td>Total Approved</td><td>Total Denied</td><td>Count Submitted</td><td>Submission Date</td></tr>";
        for (var i = 0; i < data.length; i++) {
            var d = data[i];
            
            try { d.totalsubmitted = Number(parseFloat(d.totalsubmitted).toFixed(2)).toLocaleString('en'); } catch (e) { ; }
            summary += '<tr>' +
                '<td><a id="' + d.batchnumber + '" class="btn btn-outline-primary waves-effect waves-light btn-sm view-claims prev-def" onclick="ViewPCClaims(\'' + d.batchnumber + '\')">View&nbsp;<i class="fa fa-eye"></i></a></td>' +
                '<td>' + d.batchnumber + '</td>' +
                '<td>' + FormatMoney(d.totalcharge) + '</td>' +
                '<td>' + FormatMoney(d.totalapproved) + '</td>' +
                '<td>' + FormatMoney(d.totaldenied) + '</td>' +
                '<td>' + FormatMoney(d.totalsubmitted) + '</td>' +
                '<td>' + FormatDate((d.submissiondate !== undefined) ? d.submissiondate : d.datesubmitted) + '</td></tr>'; 
        }
        summary += "</table>";
        $('.report-container').html(summary);
        $('.no-auth-report-loading').addClass('hide');
        LogEvent("Pulling Report Summary", "Pulling " + rType + " report for " + $('#no-auth-report-month').val() + "/" + $('#no-auth-report-year').val());  
    };
    var data = {
        month: $('#no-auth-report-month').val(),
        year: $('#no-auth-report-year').val(),
        reportType: reportType,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.no-auth-report-loading').removeClass('hide');
    POSTToServer2($(e).data('url'), data, success, error);
}
function ViewPCClaims(batchnumber) {
    var reportType = $('#no-auth-report-type').val();

    if (pc_ClaimsDatatlized === true) {
        $('#pc_claims').DataTable().destroy();
        $('#pc_claims_content').html('');
    }

    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
      
        if (reportType === "2")
            $('#reportTypeer').text("No-Auth Claim Items for selected batch (" + batchnumber + ")");
        else if (reportType === "1")
            $('#reportTypeer').text("Pre-Auth Claim Items for selected batch (" + batchnumber + ")");
        else
            $('#reportTypeer').text("Exclusion Claim Items for selected batch (" + batchnumber + ")");

        var summary = "";
        for (var i = 0; i < data.length; i++) {
            var d = data[i];
            
            summary += '<tr>' +
                '<td>' + (i + 1) + '</td>' +
                '<td class="hide">' + d.claimnumber + '</td>' +
                '<td>' + d.batchnumber + '</td>' +
                '<td>' + jsUcfirst(d.lastname) + '</td>' +
                '<td>' + jsUcfirst(d.firstname) + '</td>' +
                '<td>' + jsUcfirst(d.othername) + '</td>' +
                '<td>' + d.legacycode + '</td>' +
                '<td>' + d.dependantid + '</td>' +
                '<td>' + FormatDate(d.encounterdatefrom) + '</td>' +
                '<td>' + FormatDate(d.encounterdateto) + '</td>' +
                '<td>' + ((d.enroleesigned === true) ? '<label class="label label-success">True</label>' : '<label class="label label-danger">False</label>') + '</td>' +
                '<td>' + FormatMoney(d.chargeamount) + '</td>' +
                '<td>' + ((d.qty === undefined) ? '' : d.qty) + '</td>' +
                '<td>' + (isNaN(FormatMoney(d.totalcharge)) ? FormatMoney(d.chargeamount) : FormatMoney(d.totalcharge)) + '</td>' +
                '<td>' + FormatMoney(d.deniedamount) + '</td>' +
                '<td>' + FormatMoney(d.amounttopay) + '</td>' +
                '<td>' + ((d.submitted === true) ? '<label class="label label-success">True</label>' : '<label class="label label-danger">False</label>' ) + '</td>' +
                '<td>' + ((FormatDate(d.submissiondate) === undefined) ? FormatDate(d.datesubmitted) : FormatDate(d.submissiondate)) + '</td>' +
                '<td>' + d.diagnosisdesc + '</td>' +
                '<td>' + d.proceduredesc + '</td>' +
                '<td><a href="javascript:void(0);" onclick="ShowDenialReasons(this)">Denial Reasons</a></td></tr>';
        };
        
        $('#pc_claims_content').html(summary);
        setTimeout(function () {

            if (!$.fn.DataTable.isDataTable('#pc_claims')) {
                $('#pc_claims').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5', 'csvHtml5'
                    ]
                });
            }
            pc_ClaimsDatatlized = true;
            $('.no-auth-claims-report-loading').addClass('hide');
        }, 200);

        $('.report-data-container').removeClass('hide');
        $('.no-auth-claims-report-loading').addClass('hide');
        LogEvent("Pulling Report Claim", "Viewing report for batch number: " + batchnumber);  
    };
    var data = {
        month: $('#no-auth-report-month').val(),
        year: $('#no-auth-report-year').val(),
        batchnumber: batchnumber,
        reportType: reportType,
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.no-auth-claims-report-loading').removeClass('hide');
    POSTToServer2($('#claims-report-url').data('url'), data, success, error);
}
function ShowDenialReasons(e) {

    claimnumber = $($(e).parent().parent().find('td:nth-child(2)')[0]).text();
    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        NotifyDanger('Denial Reasons', data.denialReasons);
        $('.no-auth-claims-report-loading').addClass('hide');
        LogEvent("Viewing denial reasons", "Viewing denial reasons for claim number: " + claimnumber);  
    };
    var data = {
        reportType: $('#no-auth-report-type').val(),
        providerId: __ProviderId,
        claimnumber: claimnumber,
        __RequestVerificationToken: token
    };
    $('.no-auth-claims-report-loading').removeClass('hide');
    POSTToServer2($('#denial-reasons-url').data('url'), data, success, error);
}
$('body').find('.introjs-skipbutton').click(function () {
    localStorage.setItem("intro", "skipped");
    NotifySuccess('Guided Tour', 'Ok. We will skip the tour for now. Remember that you can initiate the tour at any time using the button on the top-right corner');
    LogEvent("Skipping Tour", "Tour skipped");  
});

//============================================================================================================
//============================= Bulk Upload ==================================================================
//============================== PA CLAIMS ===================================================================
$("#excelfile").click(function () { $(this).val(''); });
$("#excelfile").change(function () {
    if ($("#excelfile").val() !== "") {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
        /*Checks whether the file is a valid excel file*/
        if (!regex.test($("#excelfile").val().toLowerCase())) {
            NotifyDanger('Upload Error', "Please upload a valid Excel file!");
            return false;
        }
        else {
            var url = $('#btnUploadFile').data("url");
            UploadSelectedExcelsheet(url);
        }
    }
    else {
        alert("Please upload a Excel file!");
        return false;
    }
});

var PAClaimLines = null;
function UploadSelectedExcelsheet(url) {
    var data = new FormData();
    var i = 0;
    var fl = $("#excelfile").get(0).files[0];

    if (fl !== undefined)
        data.append("file", fl);

    $('.pre-auth-list-loading').removeClass('hide');
    $.ajax({
        type: "POST",
        url: url,
        contentType: false,
        processData: false,
        data: data,
        success: function (result) {

            var serror = "";
            var errors = "";
            if (result.entered > 0) {                
                if (result.failed > 0 && result.errors.length > 0) {
                    errors = "";
                    for (var j = 0; j < result.errors.length; j++) {
                        errors += "<small>" + result.errors[j] + "</small><br />";
                    }
                    serror = result.failed + " error(s) found.<br /><strong>Click 'Start' button below to process claims.</strong><br />"
                        + "<a class='btn btn-danger pull-right btn-sm waves-effect' style='margin-top:10px;' data-toggle='modal' data-target='#bulk-upload-progress'>View Errors</a><br />";
                    $('#progress-cont').html("<h6 class='text-danger'>Claim Line Errors:<br /></h6>" + errors);
                }
                NotifySuccess('Notification', result.entered + " claim(s) uploaded for processing.<br />" + serror + ((serAdded === true) ? "" : "<strong>Click 'Start' button below to process claims.</strong>"), (serAdded === false));
              
                $('.start').removeClass('hide');
                PAClaimLines = result.claimLines;
                $('.pre-auth-list-loading').addClass('hide');
                LogEvent("Uploading PA Claims", "PA claims Bulk upload");
            }
        },
        error: function (xhr, status, p3, p4) {
            var err = "Error " + " " + status + " " + p3 + " " + p4;
            if (xhr.responseText && xhr.responseText[0] === "{")
                err = JSON.parse(xhr.responseText).Message;
            NotifyDanger("Upload Errors", err);
            return false;
        }
    });
}
$('.start').click(function () {
    $('.pre-auth-list-loading').removeClass('hide');

    var notice = new PNotify({
        text: "Please wait",
        addclass: 'bg-primary',
        type: 'info',
        icon: 'icon-spinner4 spinner',
        hide: false,
        buttons: {
            closer: false,
            sticker: false
        },
        opacity: .9,
        width: "220px"
    });
    $('#progress-cont').text('');
    ProcessBulk(0, notice);
});
function ProcessBulk(i, notice) {
    if (PAClaimLines === null || PAClaimLines === undefined) { NotifyDanger("Notification", "No claim lines found!"); return; }
    if (i === PAClaimLines.length) {
        NotifySuccess("Notification", "Claim processing complete");
        $('.pre-auth-list-loading').addClass('hide');
        LogEvent("PA Claims Upload Complete", "Bulk PA claims processing complete");
        return;
    }
    __ProgressNotificationComplete = false;


    var error = function (data) { LOG(data); };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        $('#progress-cont').append(((data.Success === true) ? "<label class='label label-success'>Success</label>" : "<label class='label label-danger'>Failed</label>")
            + "Line " + (i + 1) + ": " + ((data.Error === undefined || data.Error.length > 0) ? "<small class='text-danger'>" + data.Error + "</small>" : "<small>" + data.Message + "</small>")
            + "<br />");

        $("#progress-cont").scrollTop($("#progress-cont")[0].scrollHeight);        

        $("#progress").removeClass('hide');
        notice.update({
            title: false
        });

        var percent = Math.ceil(((i + 1) * 100) / PAClaimLines.length);
        ++i;

        var options = {
            text: "Line " + (i + 1) + ": (" + percent + "% complete)"
        };

        if (percent === 80) options.title = "Almost There";
        if (percent >= 100) {

            $("#progress").addClass('hide');

            options.title = "Done!";
            options.addclass = "bg-success";
            options.type = "info";
            options.hide = true;
            options.buttons = {
                closer: true,
                sticker: true
            };
            options.icon = 'icon-checkmark3';
            options.opacity = 1;
            options.width = PNotify.prototype.options.width;
        }
        notice.update(options);

        ProcessBulk(i, notice);
    };
    var data = {
        ProviderId: __ProviderId,
        PANumber: PAClaimLines[i].PANumber,
        EncounterDateFrom: PAClaimLines[i].EDF,
        EncounterDateTo: PAClaimLines[i].EDT,
        ChargeAmount: PAClaimLines[i].ChargeAmount,
        __RequestVerificationToken: token
    };
    POSTToServer2($('#bulk-pa-processing').data('url'), data, success, error);
}

//============================== PC CLAIMS ===================================================================
$("#pcexcelfile").click(function () { $(this).val(''); });
$("#pcexcelfile").change(function () {
    if ($("#pcexcelfile").val() !== "") {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
        /*Checks whether the file is a valid excel file*/
        if (!regex.test($("#pcexcelfile").val().toLowerCase())) {
            NotifyDanger('Upload Error', "Please upload a valid Excel file!");
            return false;
        }
        else {
            var url = $('#btnPCUploadFile').data("url");
            UploadPCSelectedExcelsheet(url);
        }
    }
    else {
        alert("Please upload a Excel file!");
        return false;
    }
});

var pcClaimLines = null;
function UploadPCSelectedExcelsheet(url) {
    var data = new FormData();
    var i = 0;
    var fl = $("#pcexcelfile").get(0).files[0];

    if (fl !== undefined)
        data.append("file", fl);

    $('.no-auth-list-loading').removeClass('hide');
    $.ajax({
        type: "POST",
        url: url,
        contentType: false,
        processData: false,
        data: data,
        success: function (result) {

            var serror = "";
            var errors = "";
            var serAdded = false;
            if (result.entered > 0) {
                if (result.failed > 0 && result.errors.length > 0) {
                    errors = "";
                    for (var j = 0; j < result.errors.length; j++) {
                        errors += "<small>" + result.errors[j] + "</small><br />";
                    }
                    serAdded = true;
                    serror = result.failed + " error(s) found.<br /><strong>Click 'Start' button below to process claims.</strong><br />"
                        + "<a class='btn btn-danger pull-right btn-sm waves-effect' style='margin-top:10px;' data-toggle='modal' data-target='#bulk-upload-progress'>View Errors</a><br />";
                    $('#progress-cont').html("<h6 class='text-danger'>Claim Line Errors:<br /></h6>" + errors);
                }
                NotifySuccess('Notification', result.entered + " claim(s) uploaded for processing.<br />" + serror + ((serAdded === true) ? "" : "<strong>Click 'Start' button below to process claims.</strong>"), (serAdded === false));

                $('.pcstart').removeClass('hide');
                pcClaimLines = result.claimLines;
                $('.no-auth-list-loading').addClass('hide');
            }
            LogEvent("No-Auth Claims Uploaded", "Bulk PC claims processing started");
        },
        error: function (xhr, status, p3, p4) {
            var err = "Error " + " " + status + " " + p3 + " " + p4;
            if (xhr.responseText && xhr.responseText[0] === "{")
                err = JSON.parse(xhr.responseText).Message;
            NotifyDanger("Upload Errors", err);
            return false;
        }
    });
}
$('.pcstart').click(function () {
    $('.pre-auth-list-loading').removeClass('hide');

    var notice = new PNotify({
        text: "Please wait",
        addclass: 'bg-primary',
        type: 'info',
        icon: 'icon-spinner4 spinner',
        hide: false,
        buttons: {
            closer: false,
            sticker: false
        },
        opacity: .9,
        width: "220px"
    });
    $('#progress-cont').text('');
    ProcessPCBulk(0, notice);
});
function ProcessPCBulk(i, notice) {
    if (pcClaimLines === null || pcClaimLines === undefined) { NotifyDanger("Notification", "No claim lines found!"); return; }
    if (i === pcClaimLines.length) {
        NotifySuccess("Notification", "Claim processing complete");
        $('.pre-auth-list-loading').addClass('hide');
        LogEvent("PC Claims Upload Complete", "Bulk PC claims processing complete");
        return;
    }

    var error = function (data) { LOG(data); };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        $('#progress-cont').append(((data.Success === true) ? "<label class='label label-success'>Success</label>" : "<label class='label label-danger'>Failed</label>")
            + "Line " + (i + 1) + ": " + ((data.message === undefined) ? "<small class='text-danger'>" + data.result + "</small>" : "<small>" + data.message + "</small>")
            + "<br />");

        $("#progress-cont").scrollTop($("#progress-cont")[0].scrollHeight);

        $("#pcprogress").removeClass('hide');
        notice.update({
            title: false
        });

        var percent = Math.ceil(((i + 1) * 100) / pcClaimLines.length);
        ++i;

        var options = {
            text: "Line " + i + ": (" + percent + "% complete)"
        };

        if (percent === 80) options.title = "Almost There";
        if (percent >= 100) {

            $("#progress").addClass('hide');

            options.title = "Done!";
            options.addclass = "bg-success";
            options.type = "info";
            options.hide = true;
            options.buttons = {
                closer: true,
                sticker: true
            };
            options.icon = 'icon-checkmark3';
            options.opacity = 1;
            options.width = PNotify.prototype.options.width;
        }
        notice.update(options);

        ProcessPCBulk(i, notice);
    };
    var data = {
        ProviderId: __ProviderId,
        LegacyCode: pcClaimLines[i].LegacyCode,
        DependantId: pcClaimLines[i].DependantId,
        EncounterDateFrom: pcClaimLines[i].EDF,
        EncounterDateTo: pcClaimLines[i].EDT,
        ProcedureCode: pcClaimLines[i].ProcedureCode,
        DiagnosisCode: pcClaimLines[i].DiagnosisCode,
        PhysicianId: pcClaimLines[i].PhysicianId,
        EnrolleeSigned: (pcClaimLines[i].EnrolleeSigned.toLowerCase() === "yes"),
        ChargeAmount: pcClaimLines[i].ChargeAmount,
        Qty: pcClaimLines[i].Qty,
        Iter: i,
        __RequestVerificationToken: token
    };
    POSTToServer2($('#bulk-pc-processing').data('url'), data, success, error);
}
function GeneratePAPaymentAdvice() {
    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        NotifySuccess('Notification', data.message);
        $('.no-auth-list-loading').addClass('hide');
    };
    var data = {
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.no-auth-list-loading').removeClass('hide');
    POSTToServer2($('.send-pc-payment-advice').data('url'), data, success, error);
}
$('.submit-pc-claims').click(function () {
    var c = confirm("Are you sure? This process is irreversible!");
    if (!c)
        return false;

    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        NotifySuccess('Notification', data.message);
        $('.no-auth-list-loading').addClass('hide');
        GetNoAuthData(false);
        LogEvent("Submitted PC Claims", "PC Claims submission by provider");
    };
    var data = {
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.no-auth-list-loading').removeClass('hide');
    POSTToServer2($(this).data('url'), data, success, error);
});
$('.remove-pc-claims').click(function () {
    var confirm = confirm("Are you sure? This process is irreversible!");
    if (!confirm)
        return false;

    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        NotifyDanger('Notification', data.message);
        $('.no-auth-list-loading').addClass('hide');
        GetNoAuthData(false);
        LogEvent("Removing PC Claims", "PC Claims removed by provider");
    };
    var data = {
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.no-auth-list-loading').removeClass('hide');
    POSTToServer2($(this).data('url'), data, success, error);
});
function GeneratePCPaymentAdvice() {
    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        NotifySuccess('Notification', data.message);
        $('.pre-auth-list-loading').addClass('hide');
    };
    var data = {
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.pre-auth-list-loading').removeClass('hide');
    POSTToServer2($('.send-pc-payment-advice').data('url'), data, success, error);
}
$('.submit-pa-claims').click(function () {
    var c = confirm("Are you sure? This process is irreversible!");
    if (!c)
        return false;

    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        NotifySuccess('Notification', data.message);
        $('.pre-auth-list-loading').addClass('hide');
        GetPreAuthData();
        LogEvent("Submitted PA Claims", "PA Claims submission by provider");
    };
    var data = {
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.pre-auth-list-loading').removeClass('hide');
    POSTToServer2($(this).data('url'), data, success, error);
});
$('.submit-exclusions').click(function () {
    var error = function (data) {
        LOG(data);
    };
    var success = function (data) {
        try { data = window.JSON.parse(data); } catch (e) { LOG(e); }
        NotifySuccess('Notification', data.message);
        $('.exclusion-list-loading').addClass('hide');
        GetExclusionData();
        LogEvent("Submitted Excusion Claims", "Exclusion Claims submission by provider");
    };
    var data = {
        id: $($('.paBatchItemId')[0]).text(),
        providerId: __ProviderId,
        __RequestVerificationToken: token
    };
    $('.exclusion-list-loading').removeClass('hide');
    POSTToServer2($(this).data('url'), data, success, error);
});
function LogEvent(action, content) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://online.hygeiahmo.com/hygeiaapiservice/api/Audit/LogAction",
        "method": "POST",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "PTkn": "295fd65e-07a4-4ba4-b44a-00418eceff3b"
        },
        "data": {
            "Action": action,
            "Description": content,
            "TimeStamp": (new Date()).getTime(),
            "UserId": __ProviderId,
            "UserType": "Provider",
            "UserIp": $("#UserIp").val(),
            "MacAddress": $("#MacAddress").val()
        }
    };

    $.ajax(settings).done(function (response) {
    });
}

$('.log-action').click(function () {
    var action = $(this).data('action');
    var content = $(this).data('content');
    LogEvent(action, content);
});

// Methods to validate change password fields and to input into the database
$('#val1').hide();
$('#val2').hide();
$('#val3').hide();
$('#val4').hide();
$('#val5').hide();
$('#btnSubmit').hide();
  
$("input").keyup(function () {
    var password = $('#newpassword').val();
    var confirmPassword = $('#confirmnewpass').val();
    var inputPassword = $('#oldpassword').val();
    if (inputPassword == "") {
        $('#val5').show();
    }
    else {
        $('#val5').hide();
    }

    if (password.length < 6) {
        $('#val3').show();
    }
    else {
        $('#val3').hide();
    }

    if (!$.isNumeric(password)) {
        $('#val2').show();
    }
    else {
        $('#val2').hide();
    }

    if (password != confirmPassword) {
        $('#val1').show();
    }
    else {
        $('#val1').hide();
    }

    if (password == confirmPassword && $.isNumeric(password) && password.length >= 6 && inputPassword.length != 0) {
        $('#btnSubmit').show();
    }
});

var PasswordReset = function () {
    var username = $('#username').val();
    if (grecaptcha && grecaptcha.getResponse().length > 0) {
        $('#val4').hide();
    }
    else if (grecaptcha && grecaptcha.getResponse().length == 0) {
       $('#val4').show();
    }

    else {
        var oldPassword = $('#oldpassword').val();
        var newPassword = $('#confirmnewpass').val();
        var username = $('#username').val();
        var error = function (data) {
            LOG(data);
        }
        var success = function (data) {
            try { data = window.JSON.parse(data); } catch (e) { LOG(e); }

        }
        var data = {
            userid: username,
            oldpassword: oldPassword,
            newpassword: newPassword,
            __RequestVerificationToken: token
        };
        $('.pa-request-loading').removeClass('hide');
        POSTToServer2(resetPassword, data, success, error);
    }
    
}
$('#btnSubmit').click(function () {
    PasswordReset();
});
