﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace HyAdmin.Services
{
    public class Utils
    {
        public enum RegistrationType
        {
            //
            // Summary:
            //     Represents an admin account
            //Admin = 0,
            //
            // Summary:
            //     Represents an enrollee registration type
            Enrollee = 0,
            //
            // Summary:
            //     Represents a provider registration type
            Provider = 1,
            //
            // Summary:
            //     Represents a group registration type
            Group = 2,

            External = 3,

        }
    }

    public static class OnlinePayment
    {
        public static int ProductId { get; } = 2044;
        public static int PayItemId { get; } = 101;
        public static string RedirectURL { get; } = ConfigurationManager.AppSettings["InterswitchReturnURL"];
        public static string MacKey { get; } = ConfigurationManager.AppSettings["InterswitchMacKey"];

    }

    public class PlanDetails
    {
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public string DisplayName { get; set; }
        public string Category { get; set; }
        public string Region { get; set; }
        public double TBL { get; set; }
        public string PlanType { get; set; }
        public double IndividualPrice { get; set; }
        public double FamilyPrice { get; set; }
        public string Pic { get; set; }

        public PlanDetails GetPlanDefaults(int PlanId)
        {
            PlanDetails instance = this;
            switch (PlanId)
            {
                #region HyBasic
                case 167:
                    return new PlanDetails()
                    {
                        PlanId = 167,
                        IndividualPrice = 23000,
                        FamilyPrice = 0,
                        TBL = 750000,
                        PlanName = "HyBasic Plan",
                        DisplayName = "HyBasic Individual",
                        Category = "Standard Plan",
                        PlanType = "Individual",
                        Region = "Domestic",
                        Pic = "167.png"
                    };
                case 171:
                    return new PlanDetails()
                    {
                        PlanId = 171,
                        IndividualPrice = 106300,
                        FamilyPrice = 0,
                        TBL = 750000,
                        PlanName = "HyBasic Premium Plan",
                        DisplayName = "HyBasic Individual Premium",
                        Category = "Premium Plan",
                        PlanType = "Individual",
                        Region = "Domestic",
                        Pic = "171.png"
                    };
                case 168:
                    return new PlanDetails()
                    {
                        PlanId = 168,
                        IndividualPrice = 0,
                        FamilyPrice = 112000,
                        TBL = 3000000,
                        PlanName = "HyBasic Family of 4 Plan",
                        DisplayName = "HyBasic Family",
                        Category = "Standard Plan",
                        PlanType = "Family",
                        Region = "Domestic",
                        Pic = "168.png"
                    };
                case 169:
                    return new PlanDetails()
                    {
                        PlanId = 169,
                        IndividualPrice = 0,
                        FamilyPrice = 424800,
                        TBL = 3000000,
                        PlanName = "HyBasic Family of 4 Premium Plan",
                        DisplayName = "HyBasic Family Premium",
                        Category = "Premium Plan",
                        PlanType = "Family",
                        Region = "Domestic",
                        Pic = "169.png"
                    };
                #endregion

                #region Senior
                case 185:
                    return new PlanDetails()
                    {
                        PlanId = 185,
                        IndividualPrice = 93220,
                        FamilyPrice = 0,
                        TBL = 1500000,
                        PlanName = "Senior Mini Plan",
                        DisplayName = "Senior Mini",
                        Category = "Mini",
                        PlanType = "Individual",
                        Region = "Domestic",
                        Pic = "185.png",
                    };
                case 184:
                    return new PlanDetails()
                    {
                        PlanId = 184,
                        IndividualPrice = 267080,
                        FamilyPrice = 0,
                        TBL = 2500000,
                        PlanName = "Senior Midi Plan",
                        DisplayName = "Senior Midi",
                        Category = "Midi",
                        PlanType = "Individual",
                        Region = "Domestic",
                        Pic = "184.png",
                    };
                case 186:
                    return new PlanDetails()
                    {
                        PlanId = 186,
                        IndividualPrice = 600750,
                        FamilyPrice = 0,
                        TBL = 5000000,
                        PlanName = "Senior Premium Plan",
                        DisplayName = "Senior Premium",
                        Category = "Premium",
                        PlanType = "Individual",
                        Region = "Domestic",
                        Pic = "186.png",
                    };
                case 391:
                    return new PlanDetails()
                    {
                        PlanId = 391,
                        IndividualPrice = 36000,
                        FamilyPrice = 0,
                        TBL = 5000000,
                        PlanName = "HyPay-Go Plan",
                        DisplayName = "HyPay-Go",
                        Category = "HyBasic Plan",
                        PlanType = "Individual",
                        Region = "Domestic",
                        Pic = "391.png",
                    };
                case 392:
                    return new PlanDetails()
                    {
                        PlanId = 392,
                        IndividualPrice = 36000,
                        FamilyPrice = 0,
                        TBL = 5000000,
                        PlanName = "HyPay-Go Plus Plan",
                        DisplayName = "HyPay-Go Plus",
                        Category = "HyPrime Plan",
                        PlanType = "Individual",
                        Region = "Domestic",
                        Pic = "392.png",
                    };
                    #endregion
            }
            return this;
        }
        
        public string GenerateTxnNo(int RetailCustomerId, int PlanId)
        {
            string trxnNo = "";
            int idlenrem = 7 - RetailCustomerId.ToString().Length;
            #region Hide
            string rand = rand = DateTime.Now.ToString("yymmddssffaa").Replace(":", "").Replace("-", "").Replace("/", "").Replace("0", "").Substring(0, idlenrem);
            switch (PlanId.ToString())
            {
                case "167":
                    #region 167
                    trxnNo = "HYBI" + RetailCustomerId + rand;
                    break;
                #endregion
                case "171":
                    #region 171
                    trxnNo = "HYBI" + RetailCustomerId + rand;
                    break;
                #endregion
                case "168":
                    #region 168
                    trxnNo = "HYBF" + RetailCustomerId + rand;
                    break;
                #endregion
                case "169":
                    #region 169
                    trxnNo = "HYBF" + RetailCustomerId + rand;
                    break;
                #endregion
                //-------------------------------------------------------------
                case "1770":
                    #region 177
                    trxnNo = "HYST" + RetailCustomerId + rand;
                    break;
                #endregion
                case "1771":
                    #region 177
                    trxnNo = "HYST" + RetailCustomerId + rand;
                    break;
                #endregion
                case "1780":
                    #region 178
                    break;
                #endregion
                case "1781":
                    #region 178
                    trxnNo = "HYST" + RetailCustomerId + rand;
                    break;
                #endregion
                //---------------------------------------------------------------
                case "185":
                    #region 185
                    trxnNo = "SCMN" + RetailCustomerId + rand;
                    break;
                #endregion
                case "184":
                    #region 184
                    trxnNo = "SCMD" + RetailCustomerId + rand;
                    break;
                #endregion
                case "186":
                    #region 186
                    trxnNo = "SCMP" + RetailCustomerId + rand;
                    break;
                #endregion
                case "212":
                    #region 212
                    trxnNo = "BIZS" + RetailCustomerId + rand;
                    break;
                #endregion
                //---------------------------------------------------------------
                case "391":
                    #region 391
                    trxnNo = "CRDO" + RetailCustomerId + rand;
                    break;
                #endregion
                case "392":
                    #region 392
                    trxnNo = "CRDT" + RetailCustomerId + rand;
                    break;
                    #endregion
            }
            #endregion
            return trxnNo;
        }

        public IOrderedEnumerable<PlanDetails> GetAllplans()
        {
            List<PlanDetails> PlanDetails = new List<PlanDetails>();
            PlanDetails.Add(GetPlanDefaults(167));
            PlanDetails.Add(GetPlanDefaults(171));
            PlanDetails.Add(GetPlanDefaults(168));
            PlanDetails.Add(GetPlanDefaults(169));
            PlanDetails.Add(GetPlanDefaults(185));
            PlanDetails.Add(GetPlanDefaults(184));
            PlanDetails.Add(GetPlanDefaults(186));
            PlanDetails.Add(GetPlanDefaults(391));
            PlanDetails.Add(GetPlanDefaults(392));
            var res = PlanDetails.OrderBy(p => p.PlanName);
            //.Select(x => new { PlanId = x.PlanId.ToString(), PlanName = x.DisplayName });

            return res;
        }
    }

    public class OnlinePlans
    {
        public IDictionary<string, string> GetPlanDefaults(string plancode)
        {
            IDictionary<string, string> PlanDetails = new Dictionary<string, string>();
            switch (plancode)
            {
                case "HYBI":
                    PlanDetails.Add("PlanId", "167");
                    PlanDetails.Add("PlanName", "HyBasic Plan");
                    PlanDetails.Add("DisplayName", "HyBasic Individual");
                    PlanDetails.Add("Category", "Standard Plan");
                    PlanDetails.Add("Region", "Domestic");
                    PlanDetails.Add("TBL", "750000");
                    PlanDetails.Add("PlanType", "Individual");
                    PlanDetails.Add("IndividualPrice", "23000");
                    PlanDetails.Add("FamilyPrice", "0");
                    break;

                case "HYBIP":
                    PlanDetails.Add("PlanId", "171");
                    PlanDetails.Add("PlanName", "HyBasic Premium Plan");
                    PlanDetails.Add("DisplayName", "HyBasic Individual Premium");
                    PlanDetails.Add("Category", "Premium Plan");
                    PlanDetails.Add("Region", "Domestic");
                    PlanDetails.Add("TBL", "750000");
                    PlanDetails.Add("PlanType", "Individual");
                    PlanDetails.Add("IndividualPrice", "106300");
                    PlanDetails.Add("FamilyPrice", "0");
                    break;

                case "HYBF":
                    PlanDetails.Add("PlanId", "168");
                    PlanDetails.Add("PlanName", "HyBasic Family of 4 Plan");
                    PlanDetails.Add("DisplayName", "HyBasic Family");
                    PlanDetails.Add("Category", "Standard Plan");
                    PlanDetails.Add("Region", "Domestic");
                    PlanDetails.Add("TBL", "3000000");
                    PlanDetails.Add("PlanType", "Family");
                    PlanDetails.Add("IndividualPrice", "0");
                    PlanDetails.Add("FamilyPrice", "112000");
                    break;

                case "SMIN":
                    PlanDetails.Add("PlanId", "185");
                    PlanDetails.Add("PlanName", "Senior Mini Plan");
                    PlanDetails.Add("DisplayName", "Senior Mini");
                    PlanDetails.Add("Category", "Mini");
                    PlanDetails.Add("Region", "Domestic");
                    PlanDetails.Add("TBL", "1500000");
                    PlanDetails.Add("PlanType", "Individual");
                    PlanDetails.Add("IndividualPrice", "93220");
                    PlanDetails.Add("FamilyPrice", "0");
                    break;

                case "SMID":
                    PlanDetails.Add("PlanId", "184");
                    PlanDetails.Add("PlanName", "Senior Midi Plan");
                    PlanDetails.Add("DisplayName", "Senior Midi");
                    PlanDetails.Add("Category", "Midi");
                    PlanDetails.Add("Region", "Domestic");
                    PlanDetails.Add("TBL", "2500000");
                    PlanDetails.Add("PlanType", "Individual");
                    PlanDetails.Add("IndividualPrice", "267080");
                    PlanDetails.Add("FamilyPrice", "0");
                    break;

                case "SPRM":
                    PlanDetails.Add("PlanId", "186");
                    PlanDetails.Add("PlanName", "Senior Premium Plan");
                    PlanDetails.Add("DisplayName", "Senior Premium");
                    PlanDetails.Add("Category", "Premium");
                    PlanDetails.Add("Region", "Domestic");
                    PlanDetails.Add("TBL", "5000000");
                    PlanDetails.Add("PlanType", "Individual");
                    PlanDetails.Add("IndividualPrice", "600750");
                    PlanDetails.Add("FamilyPrice", "0");
                    break;

                case "HYBFP":
                    PlanDetails.Add("PlanId", "168");
                    PlanDetails.Add("PlanName", "HyBasic Family of 4 Premium Plan");
                    PlanDetails.Add("DisplayName", "HyBasic Family Premium");
                    PlanDetails.Add("Category", "Premium Plan");
                    PlanDetails.Add("Region", "Domestic");
                    PlanDetails.Add("TBL", "3000000");
                    PlanDetails.Add("PlanType", "Family");
                    PlanDetails.Add("IndividualPrice", "0");
                    PlanDetails.Add("FamilyPrice", "424800");
                    break;


                default:
                    PlanDetails.Add("PlanId", "167");
                    PlanDetails.Add("PlanName", "HyBasic Plan");
                    PlanDetails.Add("DisplayName", "HyBasic Individual");
                    PlanDetails.Add("Category", "Standard Plan");
                    PlanDetails.Add("Region", "Domestic");
                    PlanDetails.Add("TBL", "750000");
                    PlanDetails.Add("PlanType", "Individual");
                    PlanDetails.Add("IndividualPrice", "23000");
                    PlanDetails.Add("FamilyPrice", "0");
                    break;
            }
            return PlanDetails;
        }
    }
}