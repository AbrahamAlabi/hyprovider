﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HyAdmin.Services
{

    public class Service
    {

        public string JString(object obj)
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings());
        }

        public static class InvalidRequest
        {
            public static string Code { get; set; } = "Error #006";
            public static string Message { get; set; } = "Invalid Request. Required parameter is missing";
        }
        public static class RecordNotFound
        {
            public static string Code { get; set; } = "Error #005";
            public static string Message { get; set; } = "Record not found";
        }
        public static class MemberNotFound
        {
            public static string Code { get; set; } = "Error #001";
            public static string Message { get; set; } = "Member not found";
        }
        public static class EmailNotSet
        {
            public static string Code { get; set; } = "Error #002";
            public static string Message { get; set; } = "Email not set or invalid";
        }
        public static class MailDeliveryVerification
        {
            public static string Code { get; set; } = "Error #003";
            public static string Message { get; set; } = "Unable to verify mail status";
        }
        public static class UnknownError
        {
            public static string Code { get; set; } = "Error #004";
            public static string Message { get; set; } = "An unknown error occured";
        }
    }
}