﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace HyAdmin.Services
{
    public static class Utilities
    {

        internal static void WriteErrorLog(object result)
        {
            StreamWriter sw = null;
            try
            {
                string error = JsonConvert.SerializeObject(result);
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\HyDirectWebAPI.txt", true);
                sw.WriteLine(DateTime.Today.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) +
                    ": " + error);
                sw.WriteLine("");
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\HyDirectWebAPI.txt", true);
                sw.WriteLine(DateTime.Today.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) +
                    ": " + ex.Source.ToString().Trim() + ": " + ex.Message.ToString().Trim() + "\n" + ex.InnerException);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteErrorLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\HyDirectWebAPI.txt", true);
                sw.WriteLine(DateTime.Today.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
    }
}