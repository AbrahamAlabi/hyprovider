﻿using HyAdmin.AppUtilities;
using HyProvider.HDS;
using HyAdmin.Models.ViewModels;
using HyAdmin.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HyAdmin.Controllers
{
    public class MemberController : Controller
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(string searchTarget, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.MemberSearch;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"SearchTarget={searchTarget}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Eligility(int memberId, int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.Eligibility;
            
            var client = new RestClient($"{baseURL}{endpoint}/{memberId}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckIn(string legacyCode, int dependantId, int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.CheckIn;

            var client = new RestClient($"{baseURL}{endpoint}/{legacyCode}/{dependantId}/1/{providerId}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckInList(string startDate, string endDate, int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.CheckInList;

            var client = new RestClient($"{baseURL}{endpoint}/{startDate}/{endDate}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMemberIssuedPAs(string legacyCode, int dependantId, int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetMemberIssuedPAs;

            var client = new RestClient($"{baseURL}{endpoint}/{legacyCode}/{dependantId}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CoveredServices(int planId, string __RequestVerificationToken)
        {
            using (var db = new HyProvider.Models.ExistingDBs.HyDirect())
            {
                var r = db.HyDirect_Get_Plan_Covered_Benefits(planId).ToList();//.Take(5000);
                return Json(r, JsonRequestBehavior.AllowGet);
            }

            #region Hide
            //Token t = new Token();
            //t = t.GetToken("");

            //string baseURL = APIServices.BaseURL;
            //string endpoint = APIServices.CoveredServices;

            //var client = new RestClient($"{baseURL}{endpoint}/{planId}");
            //var request = new RestRequest(Method.GET);
            //request.AddHeader("cache-control", "no-cache");
            //request.AddHeader("content-type", "application/x-www-form-urlencoded");
            //request.AddHeader("authorization", "Bearer " + t.Tokn);
            //request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            //IRestResponse response = client.Execute(request);
            //return Json((response.Content), JsonRequestBehavior.AllowGet); 
            #endregion
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FindPA(int paNumber, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.FindPA;

            var client = new RestClient($"{baseURL}{endpoint}/{paNumber}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetTariff(int providerId, int memberId, string procedureCode, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetTariff;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}/{memberId}/{procedureCode}/false");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckNoAuthAccess(int providerId, string legacyCode, int dependantId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.EnrolleeNoAuthAccess;

            var client = new RestClient($"{baseURL}{endpoint}/{legacyCode}/{dependantId}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }
    }
}