﻿using HyAdmin.AppUtilities;
using HyProvider.HDS;
using HyAdmin.Models.ViewModels;
using HyAdmin.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HyAdmin.Controllers
{
    public class ReportsController : Controller
    {
        [HttpPost]
        public ActionResult ClaimsSummary(int month, int year, int reportType, int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = "";
            if (reportType == 1)
                endpoint = APIServices.SubmittedPreAuthReportSummary;
            else if (reportType == 2)
                endpoint = APIServices.SubmittedNoAuthReportSummary;
            else if (reportType == 3)
                endpoint = APIServices.SubmittedExclusionsReportSummary;

            var client = new RestClient($"{baseURL}{endpoint}/{month}/{year}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ClaimsReport(string batchnumber, int month, int year, int reportType, int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = "";
            if (reportType == 1)
                endpoint = APIServices.SubmittedPreAuthReportClaims;
            else if (reportType == 2)
                endpoint = APIServices.SubmittedNoAuthReportClaims;
            else if (reportType == 3)
                endpoint = APIServices.SubmittedExclusionsReportClaims;

            var client = new RestClient($"{baseURL}{endpoint}/{batchnumber}/{month}/{year}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DenialReasons(int reportType, int providerId, string claimnumber, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = "";
            if (reportType == 1)
                endpoint = APIServices.GetPAClaimDenialReasons;
            else if (reportType == 2)
                endpoint = APIServices.GetPCClaimDenialReasons;
            //else if (reportType == 3)
            //    endpoint = APIServices.SubmittedExclusionsReportClaims;

            var client = new RestClient($"{baseURL}{endpoint}/{claimnumber}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }

    }

    
}