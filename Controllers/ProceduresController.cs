﻿using HyAdmin.AppUtilities;
using HyProvider.HDS;
using HyAdmin.Models.ViewModels;
using HyAdmin.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HyAdmin.Controllers
{
    public class ProceduresController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index(int Id)
        {
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Search(string legacyCode, int dependantId, int providerId, string target)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.SearchProcedureByMemberProvider;

            var client = new RestClient($"{baseURL}{endpoint}/{legacyCode}/{dependantId}/{providerId}/{target}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetTariff(string legacyCode, int dependantId, int providerId, string procedureCode)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetTariffByMemberProviderProcedure;

            var client = new RestClient($"{baseURL}{endpoint}/{legacyCode}/{dependantId}/{providerId}/{procedureCode}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.Content), JsonRequestBehavior.AllowGet);
        }
    }

    
}