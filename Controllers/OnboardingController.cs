﻿using HyDirect.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.Controllers
{
    [MaintenanceFilter]
    [Authorize(Roles = "Admin,Staff")]
    public class OnboardingController : Controller
    {
        public ActionResult ProviderRequests()
        {
            return View();
        }

        public ActionResult AllProviders()
        {
            return View();
        }
    }
}