﻿using HyAdmin.AppUtilities;
using HyDirect.Filters;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.Controllers
{
    
    [MaintenanceFilter]
    [Authorize]
    public class DashboardController : Controller
    {
        public ActionResult Index()
        {
            if (Session["providername"] == null)
            {
                return RedirectToAction("Login", "Account");
            }

            Session.Timeout = 180;
            ViewBag.ProviderId = Convert.ToInt32(Session["providerid"]); ;
            ViewBag.providerKey = Convert.ToInt32(Session["providerkey"]); ;
            ViewBag.providerName = Session["providername"].ToString();

            ViewBag.UserIp = GetIpAddres();
            ViewBag.MacAddress =
                (from nic in NetworkInterface.GetAllNetworkInterfaces()
                 where nic.OperationalStatus == OperationalStatus.Up
                 select nic.GetPhysicalAddress().ToString())
                .FirstOrDefault();

            return View();
        }

        internal static string GetIpAddres()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public ActionResult AdminChat()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(string userId, string oldPassword, string newPassword, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.ResetPassword;

            var client = new RestClient($"{baseURL}{endpoint}/{userId}/{oldPassword}/{newPassword}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"userId={userId}&oldPassword={oldPassword}&newPassword={newPassword}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }
    }
}