﻿using HyAdmin.AppUtilities;
using HyProvider.HDS;
using HyAdmin.Models.ViewModels;
using HyAdmin.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using System.Data;

namespace HyAdmin.Controllers
{
    public class BillingController : Controller
    {

        #region Init
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AllDiagnosis(string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.FilteredDiagnosis;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProceduresByMemberProvider(string legacyCode, int dependantId, int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.SearchProcedureByMemberProvider;

            var client = new RestClient($"{baseURL}{endpoint}/{legacyCode}/{dependantId}/{providerId}/-");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AllProcedures(string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.AllProcedures;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }
        #endregion
        
        #region PA Requester
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RequestPA(string legacyCode, int dependantId, int providerId, string procedureDate, string diagnosisSet, string comments, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.RequestPA;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"legacyCode={legacyCode}&dependantId={dependantId}&providerId={providerId}&procedureDate={procedureDate}&diagnosisSet={diagnosisSet}&comments={comments}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPARequests(int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetPARequests;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPAIssueDetails(string requestTracker, int legacyCode, int dependantId, int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetPAIssueDetails;

            var client = new RestClient($"{baseURL}{endpoint}/{requestTracker}/{legacyCode}/{dependantId}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        } 
        #endregion
        
        #region Exclusions
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetExclusionData(int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.NonSubmittedExclusions;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExclusionSubmission(int providerId, int panumber, string procedurecode, int memberid, string encounterdatefrom, string encounterdateto, double chargeamount, bool isSingle, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.ExclusionProcessing;
            int source = (int)((isSingle) ? ClaimSource.HyDirectSingleUpload : ClaimSource.HyDirectBulkUpload);

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded",
                $"PANumber={panumber}&" +
                $"EncounterDateFrom={encounterdatefrom}&" +
                $"EncounterDateTo={encounterdateto}&" +
                $"ProcedureCode={procedurecode}&" +
                $"ProviderId={providerId}&" +
                $"Charge={chargeamount}&" +
                $"UserId={providerId}&" +
                $"SourceId={source}",
                ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitBatch(int id, int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.SubmitExclusion;
            
            var client = new RestClient($"{baseURL}{endpoint}/{id}/{providerId}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region PA Claims
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PARulesTestWithTariff(int providerId, int legacyCode, int dependantId, string procedureCode, bool isNoAuthRequest, bool isPARequest, string diagnosisCode, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.PARulesTestWithTariff;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}/{legacyCode}/{dependantId}/{procedureCode}/{isNoAuthRequest}/{isPARequest}/{diagnosisCode}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PADetails(int paNumber, int providerId, bool isExclusion, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.FetchPADetails;

            var client = new RestClient($"{baseURL}{endpoint}/{paNumber}/{providerId}/{isExclusion}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PASubmission(int endorsementkey, int panumber, double procedureamount, string procedurecode, double balance, int memberid, string legacycode, int dependantid, string diagnosis, bool submitted, string submittedby, double chargeamount, int providerid, string encounterdatefrom, string encounterdateto, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.PASubmission;

            ///{endorsementkey}/{panumber}/{procedureamount}/{procedurecode}/{balance}/{memberid}/{legacycode}/{dependantid}/{diagnosis}/{submitted}/{encounterdatefrom}/{encounterdateto}/{submittedby}/{chargeamount}/{providerid}
            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            //request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            request.AddParameter("application/x-www-form-urlencoded", $"endorsementkey={endorsementkey}&panumber={panumber}&procedureamount={procedureamount}&procedurecode={procedurecode}&balance={balance}&memberid={memberid}&legacycode={legacycode}&dependantid={dependantid}&diagnosis={diagnosis}&submitted={submitted}&submittedby={submittedby}&chargeamount={chargeamount}&providerid={providerid}&encounterdatefrom={encounterdatefrom}&encounterdateto={encounterdateto}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadExcelsheet()
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                List<ExcelPAUploadVM> claimLines = new List<ExcelPAUploadVM>();
                List<string> errors = new List<string>();
                string filePath = string.Empty;

                int total = 0;
                int entered = 0;

                if (Request.Files != null)
                {
                    #region Hide
                    string path = Server.MapPath("~/Uploads/Billing/PA/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filePath = path + Path.GetFileName("PA-" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss-ff") + Path.GetExtension(file.FileName));
                    string extension = Path.GetExtension("PA-" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss-ff") + Path.GetExtension(file.FileName));
                    file.SaveAs(filePath);

                    string conString = string.Empty;
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03.
                            conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;
                        case ".xlsx": //Excel 07 and above.
                            conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }
                    #endregion

                    conString = string.Format(conString, filePath);
                    using (OleDbConnection connExcel = new OleDbConnection(conString))
                    {
                        using (OleDbCommand cmdExcel = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                            {
                                #region Read
                                DataTable dt = new DataTable();
                                cmdExcel.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                                odaExcel.SelectCommand = cmdExcel;
                                odaExcel.Fill(dt);
                                connExcel.Close();
                                #endregion

                                if (dt.Rows.Count > 0)
                                {
                                    #region Hide
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        total++;
                                        try
                                        {
                                            Convert.ToDateTime(row["encounterdatefrom"].ToString().Trim());
                                            Convert.ToDateTime(row["encounterdateto"].ToString().Trim());
                                            claimLines.Add(new ExcelPAUploadVM
                                            {
                                                PANumber = Convert.ToInt32(row["panumber"]),
                                                EDF = Convert.ToDateTime(row["encounterdatefrom"].ToString().Trim()).ToShortDateString(),
                                                EDT = Convert.ToDateTime(row["encounterdateto"].ToString().Trim()).ToShortDateString(),
                                                ChargeAmount = Convert.ToDouble(row["chargeamount"].ToString().Trim())
                                            });
                                            entered++;
                                        }
                                        catch (Exception ex)
                                        {
                                            errors.Add($"Claim line {total} was skipped. Error: {(ex.Message.Contains("String was not recognized as a valid DateTime") ? "Please check your dates. The required format is mm/dd/yyyy" : ex.Message)}");
                                        }
                                    }
                                    return Json(new { claimLines, errors, entered, total, failed = (total - entered) }, JsonRequestBehavior.AllowGet);
                                    #endregion
                                }
                                else
                                    return Json(new { Success = false, Error = "No claim lines found!" }, JsonRequestBehavior.AllowGet);
                            }
                            //failed = total - entered;
                        }
                    }
                }
                return Json(new { Success = false, Error = "No file was found!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Success = false, Error = "No file was found!" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BulkPAProcessing(int ProviderId, int PANumber, string EncounterDateFrom, string EncounterDateTo, double ChargeAmount, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.BulkPAProcessing;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            //request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            request.AddParameter("application/x-www-form-urlencoded", $"ProviderId={ProviderId}&PANumber={PANumber}&EncounterDateFrom={EncounterDateFrom}&EncounterDateTo={EncounterDateTo}&ChargeAmount={ChargeAmount}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendPAPaymentAdvice(string providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.SendPAPaymentAdvice;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitPAClaims(string providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.SubmitPAClaims;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PC Claims
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPCClaimData(int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetPCNSClaimData;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPCNSClaimDetails(int pcClaimItemId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetPCNSClaimDetails;

            var client = new RestClient($"{baseURL}{endpoint}/{pcClaimItemId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemovePCNSClaimData(int pcClaimItemId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.RemovePCNSClaimData;

            var client = new RestClient($"{baseURL}{endpoint}/{pcClaimItemId}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCLClaimData(int providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetCLClaimData;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCLNSClaimDetails(int claimItemId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetCLNSClaimDetails;

            var client = new RestClient($"{baseURL}{endpoint}/{claimItemId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveCLNSClaimData(int endorsementkey, string claimnumber, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.RemoveCLNSClaimData;

            var client = new RestClient($"{baseURL}{endpoint}/{endorsementkey}/{claimnumber}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveClaim(string legacyCode, int dependantId, int providerId, string encounterDate, string claim, string physicianId, bool enrolleeSigned, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.ProcessBulkSerilizedPCClaims;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"legacyCode={legacyCode}&dependantId={dependantId}&providerId={providerId}&encounterDate={encounterDate}&claim={claim}&physicianId={physicianId}&enrolleeSigned={enrolleeSigned}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadPCExcelsheet()
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                List<ExcelPCUploadVM> claimLines = new List<ExcelPCUploadVM>();
                List<string> errors = new List<string>();
                string filePath = string.Empty;

                int total = 0;
                int entered = 0;

                if (Request.Files != null)
                {
                    #region Hide
                    string path = Server.MapPath("~/Uploads/Billing/PC/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filePath = path + Path.GetFileName("PC-" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss-ff") + Path.GetExtension(file.FileName));
                    string extension = Path.GetExtension("PC-" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss-ff") + Path.GetExtension(file.FileName));
                    file.SaveAs(filePath);

                    string conString = string.Empty;
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03.
                            conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;
                        case ".xlsx": //Excel 07 and above.
                            conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }
                    #endregion

                    conString = string.Format(conString, filePath);
                    using (OleDbConnection connExcel = new OleDbConnection(conString))
                    {
                        using (OleDbCommand cmdExcel = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                            {
                                #region Read
                                DataTable dt = new DataTable();
                                cmdExcel.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                                odaExcel.SelectCommand = cmdExcel;
                                odaExcel.Fill(dt);
                                connExcel.Close();
                                #endregion

                                if (dt.Rows.Count > 0)
                                {
                                    #region Hide
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        total++;
                                        try
                                        {
                                            Convert.ToDateTime(row["edf"].ToString().Trim());
                                            Convert.ToDateTime(row["edt"].ToString().Trim());
                                            claimLines.Add(new ExcelPCUploadVM
                                            {
                                                LegacyCode = row["enrolleeno"].ToString(),
                                                DependantId = Convert.ToInt32(row["dependantno"]),
                                                EDF = Convert.ToDateTime(row["edf"].ToString().Trim()).ToShortDateString(),
                                                EDT = Convert.ToDateTime(row["edt"].ToString().Trim()).ToShortDateString(),
                                                ProcedureCode = row["procedurecode"].ToString(),
                                                DiagnosisCode = row["diagnosiscode"].ToString(),
                                                PhysicianId = row["physicianid"].ToString(),
                                                EnrolleeSigned = row["enrolleesigned"].ToString(),
                                                ChargeAmount = Convert.ToDouble(row["unit charge"].ToString().Trim()),
                                                Qty = Convert.ToInt32(row["qty"].ToString().Trim()),
                                            });
                                            entered++;
                                        }
                                        catch (Exception ex)
                                        {
                                            errors.Add($"Claim line {total} was skipped. Error: {(ex.Message.Contains("String was not recognized as a valid DateTime") ? "Please check your dates. The required format is mm/dd/yyyy" : ex.Message)}");
                                        }
                                    }
                                    return Json(new { claimLines, errors, entered, total, failed = (total - entered) }, JsonRequestBehavior.AllowGet);
                                    #endregion
                                }
                                else
                                    return Json(new { Success = false, Error = "No claim lines found!" }, JsonRequestBehavior.AllowGet);
                            }
                            //failed = total - entered;
                        }
                    }
                }
                return Json(new { Success = false, Error = "No file was found!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Success = false, Error = "No file was found!" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BulkPCProcessing(int ProviderId, string LegacyCode, int DependantId, string EncounterDateFrom, string EncounterDateTo, string ProcedureCode, string DiagnosisCode, string PhysicianId, bool EnrolleeSigned, double ChargeAmount, int Qty, int Iter, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.ProcessBulkPCClaims;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"providerid={ProviderId}&legacyCode={LegacyCode}&dependantId={DependantId}&encounterDate={EncounterDateFrom}&physicianId={PhysicianId}&enrolleeSigned={EnrolleeSigned}&iter={Iter}&procedureCode={ProcedureCode}&diagnosisCode={DiagnosisCode}&amount={ChargeAmount}&qty={Qty}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendPCPaymentAdvice(string providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.SendPCPaymentAdvice;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitPCClaims(string providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.SubmitPCClaims;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemovePCClaims(string providerId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.RemovePCClaims;

            var client = new RestClient($"{baseURL}{endpoint}/{providerId}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }
        #endregion
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FindPA(int paNumber, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.FindPA;

            var client = new RestClient($"{baseURL}{endpoint}/{paNumber}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }
    }


    public class ExcelPAUploadVM
    {
        public double ChargeAmount { get; set; }
        public string EDF { get; set; }
        public string EDT { get; set; }
        public int PANumber { get; set; }
    }

    public class ExcelPCUploadVM
    {
        public double ChargeAmount { get; set; }
        public string EDF { get; set; }
        public string EDT { get; set; }
        public string LegacyCode { get; set; }
        public int DependantId { get; set; }
        public string DiagnosisCode { get; set; }
        public string ProcedureCode { get; set; }
        public string EnrolleeSigned { get; set; }
        public string PhysicianId { get; set; }
        public int Qty { get; set; }
    }

    public enum ClaimSource
    {
        HyDirectBulkUpload = 1,
        HyDirectSingleUpload = 2,

        InhouseBulkUpload = 3,
        InhouseSingleUpload = 4,

        MobileBulkUpload = 5,
        MobileSingleUpload = 6
    }
}